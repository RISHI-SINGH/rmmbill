const verifyForgotPasswordOTPReducer = (state=[],action)=>{
    if(action.type === "FORGOT_PASSWORD_OTP"){
        return action.payload;
    }else{
        return state;
    }
}

export default verifyForgotPasswordOTPReducer;