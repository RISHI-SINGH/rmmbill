const forgotPasswordReducer = (state = [], action) => {
    if (action.type === 'FORGOT_PASSWORD') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default forgotPasswordReducer;
  