const getTicketCalendarReducer = (state = [], action) => {
  if (action.type === 'GET_TICKET_CALENDAR') {
    return action.payload;
  } else {
    return state;
  }
};

export default getTicketCalendarReducer;
