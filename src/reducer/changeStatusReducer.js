const changeStatusReducer = (state = [], action) => {
    if (action.type === 'CHANGE_STATUS') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default changeStatusReducer;
  