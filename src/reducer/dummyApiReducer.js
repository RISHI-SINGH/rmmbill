const dummyApiReducer = (state = [], action) => {
    if (action.type === 'API') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default dummyApiReducer;
  