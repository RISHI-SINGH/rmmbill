const dataQueryReducer = (state = [], action) => {
    if (action.type === 'DATA_QUERY') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default dataQueryReducer;
  