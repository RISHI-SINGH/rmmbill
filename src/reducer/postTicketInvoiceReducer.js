const postTicketInvoiceReducer = (state = [], action) => {
    if (action.type === 'TICKET_INVOICE') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default postTicketInvoiceReducer;
  