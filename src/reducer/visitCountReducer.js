const visitReducerReducer = (state = [], action) => {
    if (action.type === 'COUNT') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default visitReducerReducer;
  