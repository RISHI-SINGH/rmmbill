const dispositionReducer = (state = [], action) => {
    if (action.type === 'DISPOSITION') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default dispositionReducer;
  