const getStateReducer = (state = [], action) => {
    if (action.type === 'GET_STATE') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default getStateReducer;