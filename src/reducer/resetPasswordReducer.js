const resetPasswordReducer=(state=[], action)=>{
    if(action.type === "RESET_PASSWORD"){
        return action.payload;
    }else{
        return state;
    }
}

export default resetPasswordReducer;