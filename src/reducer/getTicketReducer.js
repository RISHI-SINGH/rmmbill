const getTicketReducer = (state = [], action) => {
    if (action.type === 'GET_TICKET') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default getTicketReducer;
  