import Thunk from 'redux-thunk';
import {createStore,applyMiddleware,combineReducers} from 'redux';
import dummyApiReducer from './dummyApiReducer';
import loginReducer,{userTokenReducer} from './loginReducer';
import getTicketReducer from '../reducer/getTicketReducer';
import dispositionReducer from '../reducer/dispositonReducer';
import postTicketReducer from '../reducer/postTicketReducer';
import visitCountReducer from '../reducer/visitCountReducer';
import forgotPasswordReducer from '../reducer/forgotPasswordReducer';
import verifyForgotPasswordOTPReducer  from '../reducer/verifyForgotPasswordOTPReducer';
import resetPasswordReducer from '../reducer/resetPasswordReducer';
import getStateReducer from '../reducer/getStateReducer';
import changeStatusReducer from './changeStatusReducer';
import dataQueryReducer from './dataQueryReducer';
import postTicketInvoiceReducer from './postTicketInvoiceReducer';
import getTicketCalendarReducer from './getTicketCalendarReducer';


const reducer= combineReducers({
    dummyApiReducer,
    loginReducer,
    userTokenReducer,
    getTicketReducer,
    dispositionReducer,
    postTicketReducer,
    visitCountReducer,
    forgotPasswordReducer,
    verifyForgotPasswordOTPReducer,
    resetPasswordReducer,
    getStateReducer,
    changeStatusReducer,
    dataQueryReducer,
    postTicketInvoiceReducer,
    getTicketCalendarReducer
});

const store = createStore(reducer,applyMiddleware(Thunk));
export default store;