const postTicketReducer = (state = [], action) => {
    if (action.type === 'TICKET') {
      return action.payload;
    } else {
      return state;
    }
  };
  
  export default postTicketReducer;
  