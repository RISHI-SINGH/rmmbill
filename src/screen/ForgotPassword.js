import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';

import Header from '../component/Header';
import Button from '../component/Button';
import ErrMsg from '../component/ErrMsg';
import * as forgotPasswordAction from '../action/forgotPasswordAction';

import {images} from '../utils';

const BACKGROUND_IMAGE = images.backBannerOne;

class ForgotPassword extends Component {
  state = {activityIndicator: false, email: '', err: false, errMsg: ''};

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.forgotPasswordReducer !== this.props.forgotPasswordReducer) {
      const {status, error, message} = this.props.forgotPasswordReducer;
      let {activityIndicator, err, errMsg, email} = this.state;

      if (status === 200) {
        activityIndicator = false;
        err = false;
        errMsg = '';

        Alert.alert(
          'mBill',
          message,
          [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.navigate('VerifyOTP', {keyword: email});
              },
            },
            {
              text: 'Cancel',
              style: 'cancel',
            },
          ],
          {cancelable: true},
        );
      } else {
        activityIndicator = false;
        err = true;
        errMsg = error;
      }

      this.setState({
        activityIndicator,
        err,
        errMsg,
        email,
      });
    }
  }

  checkEmail = () => {
    let {email, err, errMsg} = this.state;
    if (email === '') {
      err = true;
      errMsg = 'Please provide an Email';
    } else {
      err = false;
      errMsg = '';
    }

    this.setState({
      err,
      errMsg,
    });
    return err;
  };

  submit = () => {
    const error = this.checkEmail();
    if (!error) {
      const {email} = this.state;
      this.setState({activityIndicator: true});
      let data = {
        keyword: email,
      };
      this.props.forgotPasswordAction(data);
    } else {
      return;
    }
  };

  render() {
    console.warn('hellloo->', this.props.forgotPasswordReducer);
    console.warn('email-->', this.state.email);
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.imgBackground} source={BACKGROUND_IMAGE}>
          <View style={styles.headerContainer}>
            <Header
              leftIcon="chevronLeft"
              headingName="Forgot Password"
              headingColor="white"
              {...this.props}
            />
          </View>
        </ImageBackground>
        <View style={styles.body}>
          <View style={styles.msgContainer}>
            <Text style={{fontSize: 18, color: 'grey'}}>
              To recover your password, you need to enter your registered email
              address or phone number.
            </Text>
          </View>
          <View style={{flex: 6}}>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Email ID or Phone Number"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value => this.setState({email: value})}
              />
            </View>
            {this.state.err && (
              <View style={{alignSelf: 'center'}}>
                <ErrMsg errMsg={this.state.errMsg} />
              </View>
            )}
            <Button
              height="7%"
              bColor="red"
              width="80%"
              mTop="2%"
              textSize={24}
              activityIndicator={this.state.activityIndicator}
              text="Submit"
              onPress={() => this.submit()}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  imgBackground: {
    height: hp('25%'),
  },
  headerContainer: {
    height: hp('10%'),
  },
  body: {
    flex: 6,
  },
  inputContainer: {
    width: wp('80%'),
    borderColor: 'red',
    borderWidth: 3,
    height: hp('9%'),
    marginTop: hp('5%'),
    justifyContent: 'center',
    borderRadius: 50,
    alignSelf: 'center',
  },
  msgContainer: {
    flex: 1,
    width: wp('80%'),
    alignSelf: 'center',
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {fontSize: 18, color: 'grey'},
});

const mapStateToProps = state => {
  return {
    forgotPasswordReducer: state.forgotPasswordReducer,
  };
};

export default connect(
  mapStateToProps,
  {...forgotPasswordAction},
)(ForgotPassword);
