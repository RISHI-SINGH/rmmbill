import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Alert
} from 'react-native';
// import {NavigationEvents} from 'react-navigation';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';

import Header from '../component/Header';
import Button from '../component/Button';
import ErrMsg from '../component/ErrMsg';
import {images} from '../utils';
 import * as resetPasswordAction from '../action/resetPasswordAction';

const BACKGROUND_IMAGE = images.backBannerOne;

class ResetPassword extends Component {
  constructor(props){
    super(props);
    this.state = {
      activityIndicator: false,
      newPass: '',
      error: false,
      errMsg: '',
      conPass: '',
      PassMatch: 'Please provide Password',
      newPassErr: false,
      conPassErr: false,
      token:''
    };
  }
componentDidMount(){
 this.setState({token:this.props.verifyForgotPasswordOTPReducer && this.props.verifyForgotPasswordOTPReducer.data})
}
componentDidUpdate(prevProps){
  if(prevProps.resetPasswordReducer != this.props.resetPasswordReducer){
    if(this.props.resetPasswordReducer.status == 200){
      Alert.alert(
        'mBill',
        this.props.resetPasswordReducer && this.props.resetPasswordReducer.message,
        [
          {
            text: 'OK',
            onPress: () => {
              this.props.navigation.navigate("Login");
            },
          },
          {
            text: 'Cancel',
            style: 'cancel',
          },
        ],
        {cancelable: false},
      );
    }
  }

}

  newPassword = () => {
    const regex = new RegExp('^(?=.*[a-z])(?=.*[0-9])(?=.{8,})');
    let {newPass, newPassErr} = this.state;
    if (newPass === '' || !regex.test(newPass)) {
      newPassErr = true;
    } else {
      newPassErr = false;
    }
    this.setState({
      newPassErr,
    });
    return newPassErr;
  };

  confirmPassword = () => {
    let {conPass, conPassErr} = this.state;
    if (conPass === '') {
      conPassErr = true;
    } else {
      conPassErr = false;
    }
    this.setState({
      conPassErr,
    });
    return conPassErr;
  };
  validate =  () => {
    const {newPass, conPass} = this.state;

    if (newPass === '' || conPass === '') {
      alert('All Fields are Mandatory.');
    } else {
      if (newPass !== conPass) {
        console.warn('IFF');
        this.setState({
          PassMatch: 'Password do not match',
          conPassErr: true,
        });
      } else {
        console.warn('ELSE');
        const conPassErr = this.confirmPassword();
        const newPassErr = this.newPassword();
        if (
          conPassErr === false &&
          newPassErr === false 
        ) {
          let data = {
            newPassword:this.state.newPass,
            confirmPassword:this.state.conPass
          }
          console.warn("data--->token--->",data,this.state.token)
          this.props.resetPasswordAction(data, this.state.token);
          this.setState({
            PassMatch: '',
            conPassErr: true,
          });
        } else {
          return;
        }
      }
    }
  };

  refreshState = async () => {
    await this.setState({
     
      conPassErr: false,
      newPassErr: false,
    });
  };

  render() {
    console.warn("hello-->",this.props.resetPasswordReducer)
    return (
      <View style={styles.container}>
        {/* <NavigationEvents onWillFocus={() => this.refreshState()} /> */}
        <ImageBackground style={styles.imgBackground} source={BACKGROUND_IMAGE}>
          <View style={styles.headerContainer}>
            <Header
              leftIcon="chevronLeft"
              headingName="Reset Password"
              headingColor="white"
              {...this.props}
            />
          </View>
        </ImageBackground>
        <View style={styles.body}>
          <View style={{flex: 6}}>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="New Password"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value =>
                  this.setState({newPass: value, newPassErr: false})
                }
                onEndEditing={() => this.newPassword()}
                secureTextEntry={true}
                value={this.state.newPass}
              />
            </View>
            {this.state.newPassErr && (
              <ErrMsg errMsg="Password must have minimum eight characters, at least one letter and one number" />
            )}
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Confirm Password"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value =>
                  this.setState({conPass: value, conPassErr: false})
                }
                onEndEditing={() => this.confirmPassword()}
                secureTextEntry={true}
                value={this.state.conPass}
              />
            </View>
           
              {this.state.conPassErr && <ErrMsg errMsg={this.state.PassMatch} />}
            <Button
              height="7%"
              bColor="red"
              width="80%"
              mTop="2%"
              textSize={24}
              text="Submit"
              onPress={() => this.validate()}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgBackground: {
    flex: 2,
  },
  headerContainer: {
    height: hp('10%'),
  },
  body: {
    flex: 6,
  },
  inputContainer: {
    width: wp('80%'),
    borderColor: 'red',
    borderWidth: 3,
    height: hp('9%'),
    marginTop: hp('2%'),
    justifyContent: 'center',
    borderRadius: 50,
    alignSelf: 'center',
  },
  msgContainer: {
    flex: 1,
    width: wp('80%'),
    alignSelf: 'center',
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {fontSize: 18, color: 'grey'},
});

const mapStateToProps = state => {
  return {
    verifyForgotPasswordOTPReducer:state.verifyForgotPasswordOTPReducer,
    resetPasswordReducer : state.resetPasswordReducer,
  };
};

export default connect(mapStateToProps,{...resetPasswordAction})(ResetPassword);
