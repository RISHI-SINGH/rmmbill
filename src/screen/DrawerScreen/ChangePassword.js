import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  Alert,
  ScroolView
} from 'react-native';
// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import {NavigationEvents} from 'react-navigation';
// import {connect} from 'react-redux';
//import * as changePasswordAction from '../../actions/changePasswordAction';
//import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../component/Header';
import Button from '../../component/Button';
import ErrMsg from '../../component/ErrMsg';
import {images,analyticFunc} from '../../utils';
// import { resetPasswordAction } from '../../actions';
const BACKGROUND_IMAGE = images.backBannerOne;

class ChangePassword extends Component {
  state = {
    activityIndicator: false,
    error: false,
    errMsg: '',
    currPass: '',
    newPass: '',
    conPass: '',
    currPassErr: false,
    newPassErr: false,
    conPassErr: false,
    PassMatch: 'Please provide Password',
  };

  moveTo = Screen => {
    this.setState(
      {
        conPass: '',
        newPass: '',
        currPass: '',
        PassMatch: '',
      },
      () => this.props.navigation.navigate(Screen),
    );
  };

  componentDidMount(){
   console.warn('hello')
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.changePasswordReducer !== this.props.changePasswordReducer) {
      console.warn('Reducer', this.props.changePasswordReducer);
      const {status, error} = this.props.changePasswordReducer;
      this.setState({status, error});
      if (
        this.props.changePasswordReducer &&
        this.props.changePasswordReducer.status === 200
      ) {
        Alert.alert(
          'Success',
          this.props.changePasswordReducer &&
            this.props.changePasswordReducer.message,
          [
            {
              text: 'OK',
              onPress: () => this.moveTo('Home'),
            }
          ],
          {cancelable: false},
        );
      } else if (
        this.props.changePasswordReducer &&
        this.props.changePasswordReducer.status === 422
      ) {
        Alert.alert(
          'Error',
          this.props.changePasswordReducer &&
            this.props.changePasswordReducer.error,
          [
            {
              text: 'OK',
              onPress: () => this.moveTo('ChangePassword'),
            },
          ],
          {cancelable: false},
        );
      } else {
        Alert.alert(
          'Error',
          'Please enter correct details.',
          [
            {
              text: 'OK',
              onPress: () => this.moveTo('ChangePassword'),
            },
          ],
          {cancelable: false},
        );
      }
    }
  }

  validate = async () => {
    const {newPass, conPass, currPass} = this.state;

    if (currPass === '' || newPass === '' || conPass === '') {
      alert('All Fields are Mandatory.');
    } else {
      if (newPass !== conPass) {
        console.warn('IFF');
        this.setState({
          PassMatch: 'Password do not match',
          conPassErr: true,
        });
      } else {
        console.warn('ELSE');
        const conPassErr = this.confirmPassword();
        const newPassErr = this.newPassword();
        const currPassErr = this.currentPassword();

        const token = await AsyncStorage.getItem('@user_token');
        this.setState({token});
        const data = {
          currentPassword: this.state.currPass,
          newPassword: this.state.newPass,
          confirmPassword: this.state.conPass,
        };
        if (
          conPassErr === false &&
          newPassErr === false &&
          currPassErr === false
        ) {
          this.props.changePasswordAction(data, this.state.token);
          this.setState({
            PassMatch: '',
            conPassErr: true,
          });
        } else {
          return;
        }
      }
    }
  };

  currentPassword = () => {
    let {currPass, currPassErr} = this.state;
    if (currPass === '') {
      currPassErr = true;
    } else {
      currPassErr = false;
    }
    this.setState({
      currPassErr,
    });
    return currPassErr;
  };

  newPassword = () => {
    const regex = new RegExp('^(?=.*[a-z])(?=.*[0-9])(?=.{8,})');
    let {newPass, newPassErr} = this.state;
    if (newPass === '' || !regex.test(newPass)) {
      newPassErr = true;
    } else {
      newPassErr = false;
    }
    this.setState({
      newPassErr,
    });
    return newPassErr;
  };

  confirmPassword = () => {
    let {conPass, conPassErr} = this.state;
    if (conPass === '') {
      conPassErr = true;
    } else {
      conPassErr = false;
    }
    this.setState({
      conPassErr,
    });
    return conPassErr;
  };

  // refreshState = async () => {
  //   await this.setState({
  //     currPassErr: false,
  //     conPassErr: false,
  //     newPassErr: false,
  //   });
  // };

  render() {
    // console.warn("currentpassword==>",this.state.currPass)
    // console.warn("newpassword==>",this.state.newPass)
    // console.warn("ConfirmPasswrd==>",this.state.conPass)
    return (
      <View style={styles.container}>
        {/* <NavigationEvents onWillFocus={() => this.refreshState()} /> */}
        <ImageBackground style={styles.imgBackground} source={BACKGROUND_IMAGE}>
          <View style={styles.headerContainer}>
            <Header
              leftIcon="chevronLeft"
              headingName="Change Password"
              headingColor="white"
              {...this.props}
            />
          </View>
        </ImageBackground>
        <View style={styles.body}>
          <View style={{flex: 6}}>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Current Password"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value =>
                  this.setState({currPass: value, currPassErr: false})
                }
                onEndEditing={() => this.currentPassword()}
                secureTextEntry={true}
                value={this.state.currPass}
              />
            </View>
            {this.state.currPassErr && (
              <ErrMsg errMsg="Please provide password" />
            )}
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="New Password"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value =>
                  this.setState({newPass: value, newPassErr: false})
                }
                onEndEditing={() => this.newPassword()}
                secureTextEntry={true}
                value={this.state.newPass}
              />
            </View>
            {this.state.newPassErr && (
              <ErrMsg errMsg="Password must have minimum eight characters, at least one letter and one number" />
            )}
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Confirm Password"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value =>
                  this.setState({conPass: value, conPassErr: false})
                }
                onEndEditing={() => this.confirmPassword()}
                secureTextEntry={true}
                value={this.state.conPass}
              />
            </View>
            {this.state.conPassErr && <ErrMsg errMsg={this.state.PassMatch} />}
            <Button
              height="7%"
              bColor="red"
              width="80%"
              mTop="2%"
              textSize={24}
              text="Submit"
              onPress={() => this.validate()}
            />
            <View style={{flex: 1, alignItems: 'center', marginTop: 5}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("ForgotPassword")}>
                <Text
                  style={{color: '#8FB8FE', textDecorationLine: 'underline'}}>
                  Forgot Password
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  imgBackground: {
    flex: 2,
  },
  headerContainer: {
    height: hp('10%'),
  },
  body: {
    flex: 6,
  },
  inputContainer: {
    width: wp('80%'),
    borderColor: 'red',
    borderWidth: 3,
    height: hp('9%'),
    marginTop: hp('2%'),
    justifyContent: 'center',
    borderRadius: 50,
    alignSelf: 'center',
  },
  msgContainer: {
    flex: 1,
    width: wp('80%'),
    alignSelf: 'center',
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {fontSize: 18, color: 'grey'},
});

// const mapStateToProps = state => {
//   return {
//     changePasswordReducer: state.changePasswordReducer,
//   };
// };

// export default connect(mapStateToProps, {...changePasswordAction})(
//   ChangePassword,
// );

export default ChangePassword;