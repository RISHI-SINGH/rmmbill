import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  ScrollView,
  Alert,
} from 'react-native';
import Header from '../component/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Picker from '../component/Picker';
import AsyncStorage from '@react-native-community/async-storage';
import * as getTicketAction from '../action/getTicketAction';
import {connect} from 'react-redux';
import * as getStateAction from '../action/getStateAction';
import {alert} from '../utils';
import * as changeStatusAction from '../action/changeStatusAction';
import moment from 'moment';
import {images} from '../utils';
import * as loginAction from '../action/loginAction';

const {logoutIcon} = images;
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      List: [
        {label: 'Assigned', value: 1},
        {label: 'Overdue', value: 2},
        {label: 'Travel', value: 3},
        {label: 'Onsite', value: 4},
        {label: 'Complete', value: 5},
      ],
      id: 1,

      token: '',
      listData: [],
      status: 1,
      loader: false,
      states: '',
      isLoading: false,
    };
  }
  setFields = (key, value) => {
    console.warn(key, value);
    const err = key + 'Err';
    this.setState(
      {
        [key]: value,
        [err]: false,
        status: value,
      },
      () => {
        if (value === 1) {
          console.warn('Assigned');
          this.setState({loader: true});
          this.props.getTicketAction(this.state.token);
        } else if (value === 2) {
          console.warn('Overdue');
          this.setState({loader: true});
          this.props.getTicketAction(this.state.token);
        } else if (value === 3) {
          console.warn('travel');
          this.setState({loader: true});
          this.props.getTicketAction(this.state.token);
        } else if (value === 4) {
          console.warn('onsite');
          this.setState({loader: true});
          this.props.getTicketAction(this.state.token);
        } else if (value === 5) {
          console.warn('complete');
          this.setState({loader: true});
          this.props.getTicketAction(this.state.token);
        } else if (value === null) {
          this.setState({listData: [], loader: false});
        }
      },
    );
  };

  async componentDidMount() {
    const token = await AsyncStorage.getItem('@user_token');
    this.setState({token});
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      this.props.getTicketAction(token);
    });
    this.props.getStateAction();

    this.setState({loader: true});
    this.props.getTicketAction(token);
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  componentDidUpdate(PrevProps) {
    if (PrevProps.getTicketReducer != this.props.getTicketReducer) {
      const {data, status, message, error} = this.props.getTicketReducer;
      if (status === 200) {
        this.setState({listData: data, loader: false});
        data.map(item => {
          if (item.rmExpiryDate && item.currentStatusRm === 1) {
            let expDate = moment(item.rmExpiryDate).format('YYYY-MM-DD');
            console.warn('hellodate-->', item.ticketNumber);

            let today = moment().format('YYYY-MM-DD');
            if (today > expDate) {
              const data = {
                ticketNumber: item.ticketNumber,
              };

              console.warn('ChangeStatusRequested--->', data, expDate, today);
              this.setState({isLoading: true});
              this.props.changeStatusAction(data, this.state.token);
            }
          }
        });
      }
    }
    if (PrevProps.getStateReducer != this.props.getStateReducer) {
      const {data, status, message, error} = this.props.getStateReducer;
      if (status === 200) {
        this.setState({states: data});
      } else {
        alert(error);
      }
    }

    if (PrevProps.changeStatusReducer != this.props.changeStatusReducer) {
      const {data, status, error, message} = this.props.changeStatusReducer;
      if (status === 200) {
        this.setState({isLoading: false});
      } else {
        this.setState({isLoading: false});
      }
    }
  }

  nextScreen = item => {
    if (item.currentStatusRm !== 4) {
      this.props.navigation.navigate('TaskDetail', {
        data: item,
      });
    } else {
      this.props.navigation.navigate('TaskDetail4', {
        data: item,
      });
    }
  };

  logOut = () => {
    Alert.alert(
      'LOGOUT',
      'Are you sure?',
      [
        {
          text: 'CANCEL',
        },

        {
          text: 'OK',
          onPress: () => {
            AsyncStorage.removeItem('@user_token');
            this.props.userToken(null);
          },
        },
      ],
      {cancelable: false},
    );
  };
  render() {
    console.warn('isLoading--->', this.state.isLoading);
    var weekDayName = moment('2020-07-23').format('dddd');
    console.warn('dayy-->', weekDayName);
    console.warn(
      'Listtttt>',
      JSON.stringify(this.state.listData, undefined, 2),
    );
    console.warn('count-->', this.state.listData.length);
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            height: hp('10%'),
            flexDirection: 'row',
            marginTop: hp('5%'),
          }}>
          <View
            style={{
              height: hp('10%'),
              width: wp('85%'),
              backgroundColor: 'white',
            }}>
            <Picker
              data={this.state.List}
              mTop="0%"
              pickerFunc={value => this.setFields('id', value)}
            />
          </View>
          <TouchableOpacity
            onPress={this.logOut}
            style={{
              height: hp('10%'),
              width: wp('15%'),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
            }}>
            <Image
              source={logoutIcon}
              style={{height: 30, width: 30}}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <ScrollView>
          {this.state.listData.length === 0 ? (
            <View
              style={{
                height: hp('10%'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>Please Select the Type</Text>
            </View>
          ) : this.state.loader === true ? (
            <ActivityIndicator size="large" color="orange" />
          ) : (
            this.state.listData.map((item, index) => {
              if (item.currentStatusRm === this.state.status) {
                return this.state.isLoading ? null : (
                  <TouchableOpacity
                    onPress={() => this.nextScreen(item)}
                    style={{
                      height: hp('10%'),
                      backgroundColor: '#ffffff',
                      marginTop: hp('0.5%'),
                      marginBottom: hp('1%'),
                      width: wp('95%'),
                      alignSelf: 'center',
                      borderRadius: 15,
                      elevation: 5,
                      borderWidth: 1,
                      flexDirection: 'row',
                    }}>
                    <View style={{height: hp('10%'), width: wp('55%')}}>
                      <View
                        style={{
                          height: hp('4%'),
                          width: wp('55%'),
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            marginLeft: wp('5%'),
                          }}>
                          Type: {item.ticketType}
                        </Text>
                      </View>
                      <View
                        style={{
                          height: hp('3%'),
                          width: wp('55%'),

                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontSize: 12,
                            marginLeft: wp('5%'),
                            alignSelf: 'center',
                          }}>
                          City :
                        </Text>
                        <Text style={{fontSize: 12, alignSelf: 'center'}}>
                          {item.City}
                        </Text>
                      </View>
                      <View
                        style={{
                          height: hp('3%'),
                          width: wp('55%'),

                          flexDirection: 'row',
                        }}>
                        <Text style={{fontSize: 12, marginLeft: wp('5%')}}>
                          Name :
                        </Text>
                        <Text style={{fontSize: 12}}>
                          {item.contactPersonName}
                        </Text>
                        {/* <Text style={{fontSize: 12, marginLeft: wp('5%')}}>
                          RM :
                        </Text>
                        <Text style={{fontSize: 12}}>{item.rmId}</Text> */}
                      </View>
                    </View>
                    <View
                      style={{
                        height: hp('10%'),
                        width: wp('40%'),
                      }}>
                      <View
                        style={{
                          height: hp('6%'),
                          width: wp('40%'),
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                            color: 'red',
                          }}>
                          {item.rmExpiryDate}
                        </Text>
                      </View>
                      <View
                        style={{
                          height: hp('4%'),
                          width: wp('40%'),
                          alignItems: 'center',
                        }}>
                        <Text style={{fontSize: 10}}>Due Date</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }
            })
          )}
        </ScrollView>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              height: hp('8%'),
              backgroundColor: 'green',
              width: wp('50%'),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ED4F34',
              elevation: 10,
            }}>
            <Text style={{fontSize: 16, color: '#fff', fontWeight: 'bold'}}>
              TASK LIST{' '}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('CalendarTask', {
                status: this.state.status,
              })
            }
            style={{
              height: hp('8%'),

              width: wp('50%'),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#fff',
              elevation: 10,
            }}>
            <Text style={{fontSize: 16, color: '#ED4F34', fontWeight: 'bold'}}>
              CALENDAR{' '}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    getTicketReducer: state.getTicketReducer,
    getStateReducer: state.getStateReducer,
    changeStatusReducer: state.changeStatusReducer,
    loginReducer: state.loginReducer,
  };
};

export default connect(
  mapStateToProps,
  {
    ...getTicketAction,
    ...getStateAction,
    ...changeStatusAction,
    ...loginAction,
  },
)(Home);
