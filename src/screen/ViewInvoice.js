import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Header from '../component/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import * as dataQueryAction from '../action/dataQueryAction';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import Modal from 'react-native-modal';
import * as postTicketInvoice from '../action/postTicketInvoice';
import {alert} from '../utils';

class ViewInvoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{name: 'rishi'}],
      token: '',
      ticketNumber: '',
      invoiceData: [],
      isVisible: false,
      modalData: '',
      correctedPrice: '',
      correctedQuantity: '',
      correctedModel: '',
      dispositionId: '',
      lat: '',
      long: '',
      remark: '',
      billData: [],
      apiData: '',
      loader: false,
      spinner: true,
    };
  }

  componentDidMount = async () => {
    const token = await AsyncStorage.getItem('@user_token');
    this.setState({token});

    this.setState({
      ticketNumber: this.props.route.params.ticketNumber,
      dispositionId: this.props.route.params.dispositionId,
      lat: this.props.route.params.lat,
      long: this.props.route.params.long,
      remark: this.props.route.params.remark,
    });

    const data = {
      ticketNumber: this.state.ticketNumber,
    };
    console.warn('helloddataqqq-->', data);
    this.props.dataQueryAction(data, token);
  };

  componentDidUpdate = prevProps => {
    if (prevProps.dataQueryReducer != this.props.dataQueryReducer) {
      const {data, status, message, error} = this.props.dataQueryReducer;
      if (status === 200) {
        let invoiceData = [];
        invoiceData = data;
        invoiceData = invoiceData.map((item, index) => {
          return {...item, bool: false};
        });
        this.setState({invoiceData});
      }
    }
    if (
      prevProps.postTicketInvoiceReducer != this.props.postTicketInvoiceReducer
    ) {
      const {
        data,
        status,
        error,
        message,
      } = this.props.postTicketInvoiceReducer;
      if (status === 200) {
        Alert.alert(
          'mBill',
          message,
          [
            {
              text: 'OK',
              onPress: () => this.props.navigation.navigate('Home'),
            },
          ],
          {cancelable: false},
        );
      } else {
        Alert.alert(
          'mBill',
          error,
          [
            {
              text: 'OK',
              //onPress: () => alert('ok'),
            },
          ],
          {cancelable: false},
        );
      }
    }
  };
  openModal = (item, index) => {
    console.warn('index-->', index);
    this.setState({isVisible: true, modalData: item, modalIndex: index});
  };

  dismiss = () => {
    this.setState({isVisible: false});
  };

  saveHandle = (queryId, productId, index) => {
    const {
      correctedQuantity,
      correctedPrice,
      correctedModel,
      dispositionId,
      ticketNumber,
      lat,
      long,
      remark,
    } = this.state;
    if (
      correctedQuantity === '' ||
      correctedPrice === '' ||
      correctedModel === ''
    ) {
      Alert.alert(
        'mBill',
        'Please fill all the fields.',
        [
          {
            text: 'OK',
            //onPress: () => alert('ok'),
          },
        ],
        {cancelable: false},
      );
    } else {
      let invoiceData = [...this.state.invoiceData];
      invoiceData[index].bool = !invoiceData[index].bool;

      let items = {
        invoiceProductId: productId,
        correctPrice: correctedPrice,
        correctQuantity: correctedQuantity,
        correctModel: correctedModel,
        dataQueriesId: queryId,
      };

      let billData = [...this.state.billData];
      billData.push(items);
      console.warn('datata', billData);
      this.setState({billData}, () => {
        const data = {
          invoiceData: this.state.billData,
          ticketNumber: ticketNumber,
          location: {lat: lat, lon: long},
          dispositionId: dispositionId,
          rmRemark: remark,
        };
        this.setState({apiData: data, isVisible: false, invoiceData}, () => {
          this.setState({
            correctedPrice: '',
            correctedQuantity: '',
            correctedModel: '',
          });
        });
      });
    }
  };

  submitHandle = () => {
    if (this.state.invoiceData.length === this.state.billData.length) {
      console.warn('data-->',this.state.apiData)
      this.props.postTicketInvoice(this.state.apiData, this.state.token);
    } else {
      this.state.invoiceData.map((item, index) => {
        if (item.bool == false) {
          Alert.alert('mBill', `Required Changes invoice ${item.invoiceId}`, [
            {
              text: 'OK',
            },
          ]);
        }
        return false;
      });
    }

    //   if (this.state.billData.length > 0) {
    //     this.props.postTicketInvoice(this.state.apiData, this.state.token);
    //   } else {

    //     Alert.alert('mBill', 'Required Changes invoice`.', [
    //       {
    //         text: 'OK',
    //       },
    //     ]);
    //   }
  };

  render() {
    const {
      modalData,
      correctedModel,
      correctedPrice,
      correctedQuantity,
      modalIndex,
    } = this.state;

    console.warn('Datatata-->', this.state.remark);
    console.warn('billlll-->', this.state.billData);

    return (
      <View style={{flex: 1}}>
        <View style={{height: hp('10%')}}>
          <Header
            leftIcon="chevronLeft"
            headingName="Invoices"
            {...this.props}
          />
        </View>

        <View style={{flex: 9}}>
          {this.state.invoiceData.length > 0 ? (
            this.state.invoiceData.map((item, index) => {
              return (
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      this.openModal(item, index);
                    }}
                    style={{
                      height: hp('10%'),
                      elevation: 5,
                      backgroundColor: '#ffffff',
                      flexDirection: 'row',
                      marginBottom: wp('1.5%'),
                    }}>
                    <View style={{height: hp('10%'), width: wp('55%')}}>
                      <View
                        style={{
                          height: hp('5%'),

                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                        }}>
                        <Text style={{marginLeft: wp('3%'), fontSize: 18}}>
                          Invoice Id :{' '}
                        </Text>
                        <Text style={{fontSize: 16}}>{item.invoiceId} </Text>
                      </View>
                      <View
                        style={{
                          height: hp('5%'),

                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                          alignItems: 'center',
                        }}>
                        <Text style={{marginLeft: wp('3%'), fontSize: 18}}>
                          Mobile No. :{' '}
                        </Text>
                        <Text style={{fontSize: 16}}>{item.mobileNumber} </Text>
                      </View>
                    </View>
                    <View
                      style={{
                        height: hp('10%'),
                        width: wp('45%'),

                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: 22,
                          color: 'red',
                          fontWeight: 'bold',
                        }}>
                        {item.invoice_date}
                      </Text>
                      <Text style={{fontSize: 16}}>Invoice Date</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              );
            })
          ) : (
            <ActivityIndicator size="large" color="#ED4F2A" />
          )}
        </View>
        {this.state.loader ? (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ED4F2A',
            }}>
            <ActivityIndicator size="large" color="#ffffff" />
          </View>
        ) : (
          <TouchableOpacity
            onPress={this.submitHandle}
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ED4F2A',
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#fff'}}>
              SUBMIT
            </Text>
          </TouchableOpacity>
        )}
        <Modal
          isVisible={this.state.isVisible}
          onBackButtonPress={this.dismiss}>
          <TouchableWithoutFeedback onPress={this.dismiss}>
            <View style={styles.modalOverlay} />
          </TouchableWithoutFeedback>
          <View
            style={{
              backgroundColor: 'white',
              padding: 20,
              borderRadius: 10,
            }}>
            <View
              onPress={() => {
                this.openModal(item);
              }}
              style={{
                height: hp('10%'),
                width: wp('80%'),
                elevation: 5,
                backgroundColor: '#fff',
                flexDirection: 'row',
              }}>
              <View style={{height: hp('10%'), width: wp('40%')}}>
                <View
                  style={{
                    height: hp('5%'),

                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Text style={{marginLeft: wp('2%'), fontSize: 14}}>
                    Invoice Id :{' '}
                  </Text>
                  <Text style={{fontSize: 14}}>{modalData.invoiceId}</Text>
                </View>
                <View
                  style={{
                    height: hp('5%'),

                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                  }}>
                  <Text style={{marginLeft: wp('2%'), fontSize: 14}}>
                    Mobile No. :{' '}
                  </Text>
                  <Text style={{fontSize: 14}}>{modalData.mobileNumber}</Text>
                </View>
              </View>
              <View
                style={{
                  height: hp('10%'),
                  width: wp('40%'),

                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 20,
                    color: 'red',
                    fontWeight: 'bold',
                  }}>
                  {modalData.invoice_date}
                </Text>
                <Text style={{fontSize: 16}}>Invoice Date</Text>
              </View>
            </View>
            <View style={{height: hp('10%'), width: wp('80%')}}>
              <View style={{height: hp('3%')}}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  {' '}
                  Price -{' '}
                </Text>
              </View>
              <View style={{height: hp('3%'), flexDirection: 'row'}}>
                <View style={{height: hp('3%'), width: wp('40%')}}>
                  <Text style={{fontSize: 16}}>Original Price</Text>
                </View>
                <View style={{height: hp('3%'), width: wp('40%')}}>
                  <Text style={{fontSize: 16}}>{modalData.originalPrice}</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    height: hp('4%'),
                    width: wp('40%'),
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: 16}}>Corrected Price</Text>
                </View>
                <View
                  style={{
                    height: hp('5%'),
                    width: wp('40%'),
                    borderWidth: 1,
                    justifyContent: 'center',
                  }}>
                  <TextInput
                    placeholder="Price(Rs.)"
                    value={correctedPrice}
                    keyboardType="number-pad"
                    returnKeyType="done"
                    onChangeText={text => this.setState({correctedPrice: text})}
                  />
                </View>
              </View>
            </View>
            <View style={{height: hp('10%'), width: wp('80%')}}>
              <View style={{height: hp('3%')}}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  {' '}
                  Quantity -{' '}
                </Text>
              </View>
              <View style={{height: hp('3%'), flexDirection: 'row'}}>
                <View style={{height: hp('3%'), width: wp('40%')}}>
                  <Text style={{fontSize: 16}}>Original Quantity</Text>
                </View>
                <View style={{height: hp('3%'), width: wp('40%')}}>
                  <Text style={{fontSize: 16}}>
                    {modalData.originalQuantity}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    height: hp('4%'),
                    width: wp('40%'),
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: 16}}>Corrected Quantity</Text>
                </View>
                <View
                  style={{
                    height: hp('5%'),
                    width: wp('40%'),
                    borderWidth: 1,
                    justifyContent: 'center',
                  }}>
                  <TextInput
                    placeholder="Quantity"
                    value={correctedQuantity}
                    keyboardType="number-pad"
                    returnKeyType="done"
                    onChangeText={text =>
                      this.setState({correctedQuantity: text})
                    }
                  />
                </View>
              </View>
            </View>
            <View style={{width: wp('80%')}}>
              <View style={{height: hp('3%')}}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  Model Name -{' '}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{height: hp('3%'), width: wp('40%')}}>
                  <Text style={{fontSize: 16}}>Original Model</Text>
                </View>
                <View style={{width: wp('40%')}}>
                  <Text style={{fontSize: 16}}>{modalData.originalModel}</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View
                  style={{
                    height: hp('4%'),
                    width: wp('40%'),
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: 16}}>Corrected Model</Text>
                </View>
                <View
                  style={{
                    height: hp('5%'),
                    width: wp('40%'),
                    borderWidth: 1,
                    justifyContent: 'center',
                  }}>
                  <TextInput
                    placeholder="Model Name"
                    value={correctedModel}
                    onChangeText={text => this.setState({correctedModel: text})}
                  />
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.saveHandle(
                  modalData.dataQueriesId,
                  modalData.invoiceProductId,
                  modalIndex,
                )
              }
              style={{
                height: hp('5%'),
                width: wp('50%'),
                alignSelf: 'center',

                marginTop: hp('3%'),
                borderRadius: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ED4F2A',
              }}>
              <Text style={{fontSize: 16, color: '#fff'}}> Save</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  modalOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0)',
  },
});

const mapStateToProps = state => {
  return {
    dataQueryReducer: state.dataQueryReducer,
    postTicketInvoiceReducer: state.postTicketInvoiceReducer,
  };
};

export default connect(
  mapStateToProps,
  {...dataQueryAction, ...postTicketInvoice},
)(ViewInvoice);
