import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  Alert,
  ScrollView,
} from 'react-native';
// import {getUniqueId, getManufacturer} from 'react-native-device-info';
import {connect} from 'react-redux';
// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import {
  images,
  colors,
  getDeviceToken,
  loginWithGoogle,
  loginWithFb,
  alert,
} from '../utils';
// import {SignInWithAppleButton} from 'react-native-apple-authentication';
import * as loginAction from '../action/loginAction';

import Button from '../component/Button';
import InputText from '../component/InputText';
import ErrMsg from '../component/ErrMsg';
// import {
//   LoginManager,
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
// } from 'react-native-fbsdk';

const LOGO = images.logo;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      usernameErr: false,
      passwordErr: false,
      err: false,
      errMsg: '',
      deviceToken: '',
      activityIndicator: false,
      fbIndicator: false,
      googleIndicator: false,
      deviceType: '',
      facebookId: '',
      loginType: '',
      socialId: '',
      activityIndicatorSocial: false,
      activityIndicatorSocialFb: false,
      firstName: '',
      lastName: '',
      email: '',
      deviceId: '',
    };
    // this.onNavigateTo = this.onNavigateTo.bind(this);
  }

  componentDidMount = async () => {
    // let Id = getUniqueId();
    // this.setState({deviceId: Id});
    // console.warn('iddd-->', Id);
    // let {deviceType} = this.state;
    // if (Platform.OS === 'android') {
    //   deviceType = 'android';
    // } else {
    //   deviceType = 'ios';
    // }
    // const deviceToken = await getDeviceToken();
    // if (deviceToken) {
    //   this.setState({
    //     deviceToken,
    //     deviceType,
    //   });
    // }
  };

  componentDidUpdate = async (prevProps, prevState) => {
    if (prevProps.loginReducer !== this.props.loginReducer) {
      this.setState({
        activityIndicator: false,
        activityIndicatorSocial: false,
        activityIndicatorSocialFb: false,
      });
      const {status, error, data} = this.props.loginReducer;
      let {err, errMsg} = this.state;
      if (status === 200) {
        if (data.userType == 'Rm') {
          const {data} = this.props.loginReducer;
          const {userToken, userData} = data;

          this.props.userToken(userToken);
          await AsyncStorage.setItem('@user_token', userToken);
          await AsyncStorage.setItem('@user_data', JSON.stringify(userData));
          this.goToScreen('Home');
        } else {
          Alert.alert(
            'mBill',
            'Invalid Credentials.',
            [
              {
                text: 'OK',
              },
            ],
            {cancelable: false},
          );
        }
      } else if (status === 300) {
        Alert.alert(
          'mBill',
          this.props.loginReducer && this.props.loginReducer.error,
          [
            {
              text: 'OK',
            },
          ],
          {cancelable: false},
        );
      } else {
        err = true;
        errMsg = error;
        this.setState({
          err,
          errMsg,
          activityIndicatorSocial: false,
        });
      }
    }
  };

  goRegister = async () => {
    await AsyncStorage.removeItem('@userData');
    this.props.navigation.navigate('Signup', {
      socialId: this.state.socialId,
      loginType: this.state.loginType,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
    });
  };

  fbLogin = () => {
    this.setState({
      activityIndicatorSocialFb: true,
    });
    console.log('login with fb function called');
    LoginManager.logInWithPermissions([
      'public_profile',
      'email',
      'user_friends',
    ]).then(
      result => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.warn('Login success with permissions: ' + result);
          AccessToken.getCurrentAccessToken().then(data => {
            console.warn('FB DATA', data);
            this.setState({
              accessToken: data.accessToken,
            });
            //initialise graph request
            const infoRequest = new GraphRequest(
              '/me?fields=email,id,first_name,last_name',
              null,
              this._responseInfoCallback,
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      },
      function(error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  _responseInfoCallback = (error, result) => {
    if (error) {
      alert('Error fetching data: ' + error.toString());
    } else {
      console.log('Response', result);
      this.setState(
        {
          facebookId: result.id,
        },
        () => this.loginFunction(),
      );
    }
  };

  loginFunction = () => {
    const {facebookId, deviceToken} = this.state;

    const userData = {
      facebookId,
    };

    if (this.state.facebookId) {
      this.props.userLogin({...userData, deviceToken}, 'FACEBOOK');
    }
  };

  loginWithGoogleFunc = async () => {
    const userData = await loginWithGoogle();
    console.warn('USERDATA ======>', userData);

    if (userData === undefined) {
      this.setState({
        activityIndicatorSocial: false,
      });
    } else if ('user' in userData) {
      const {user} = userData;
      this.setState({
        firstName: user.givenName,
        lastName: user.familyName,
        email: user.email,
      });
      this.setState({activityIndicatorSocial: true});
      console.warn('USERRRRRR');
      this.props.userLogin(
        {...userData, deviceToken: this.state.deviceToken},
        'GOOGLE',
      );
    } else {
      this.setState({
        activityIndicatorSocial: false,
      });
    }
  };

  appleSignIn = result => {
    console.warn('Resssult', result);
    if (result === undefined) {
      this.setState({
        activityIndicatorSocial: false,
      });
    } else if ('user' in result) {
      const {user} = result;
      this.setState({
        firstName: result.givenName,
        lastName: result.familyName,
        email: result.email,
      });
      this.setState({activityIndicatorSocial: true});
      console.warn('USERRRRRR');
      this.props.userLogin(
        {...result, deviceToken: this.state.deviceToken},
        'APPLE',
      );
    } else {
      this.setState({
        activityIndicatorSocial: false,
      });
    }
  };

  // loginWithFacebookFunc = async () => {
  //   // await loginWithFb();
  //   const userData = await loginWithFb();
  //   console.warn('USERDATA', userData);

  //   // const userData = await loginWithFb();
  //   // this.props.userLogin(
  //   //   {...userData, deviceToken: this.state.deviceToken},
  //   //   'FACEBOOK',
  //   // );
  // };

  // fbLogin = () => {
  //   alert("FB")
  // }

  goToScreen = screen => {
    this.setState({
      username: '',
      password: '',
      usernameErr: false,
      passwordErr: false,
      err: false,
      errMsg: '',
    });
    this.props.navigation.navigate(screen);
  };

  checkUsername = () => {
    let {username, usernameErr} = this.state;
    if (username === '') {
      usernameErr = true;
    } else {
      usernameErr = false;
    }
    this.setState({
      usernameErr,
    });
    return usernameErr;
  };

  checkPassword = () => {
    const regex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})',
    );
    let {password, passwordErr} = this.state;
    if (password === '') {
      passwordErr = true;
    } else {
      passwordErr = false;
    }
    this.setState({
      passwordErr,
    });
    return passwordErr;
  };

  setFields = (key, value) => {
    this.setState({
      [key]: value.trim(),
    });
  };

  submit = () => {
    const usernameErr = this.checkUsername();
    const passwordErr = this.checkPassword();

    if (usernameErr === false && passwordErr === false) {
      this.setState({
        activityIndicator: true,
      });

      const data = {
        email: this.state.username,
        password: this.state.password,
        deviceType: 'Android',
        deviceToken: 'fvghsdvfh2345',
        loginType: 'Custom',
        socialId: '354',
      };
      this.props.loginAction(data);
    } else {
      return;
    }
  };

  render() {
    console.warn('REGISTER DATA', this.state.socialId, this.state.loginType);
    console.warn(
      'fistName, lastName, email',
      this.state.firstName,
      this.state.lastName,
      this.state.email,
    );
    console.warn('hello--->', this.state.deviceToken);
    console.warn('hello123--->', this.state.deviceType);

    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <View style={{height: hp('10%'), width: wp('50%')}}>
            <Image
              source={LOGO}
              style={{height: '100%', width: '100%'}}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={styles.mainContainer}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="handled">
            <View style={styles.innerContainer}>
              <View style={styles.formContainer}>
                <View style={styles.formHeading}>
                  <Text
                    style={{
                      fontSize: 26,
                      fontWeight: 'bold',
                      marginTop: hp('1%'),
                    }}>
                    Login
                  </Text>
                  <Text style={styles.greyText}>
                    Welcome back, Kindly enter your login details
                  </Text>
                </View>
                <View style={{marginTop: hp('3%')}}>
                  <InputText
                    formText="Email or Username"
                    mTop="0"
                    value={this.state.username}
                    func={value => this.setFields('username', value)}
                    onChangeTextValue={() => this.checkUsername()}
                    style={inputStyles1}
                  />
                  {this.state.usernameErr && (
                    <ErrMsg errMsg={'Please provide email/username'} />
                  )}
                  <InputText
                    formText="Password"
                    mTop="3%"
                    value={this.state.password}
                    func={value => this.setFields('password', value)}
                    onChangeTextValue={() => this.checkPassword()}
                    secureTextEntry={true}
                    style={inputStyles1}
                  />
                  {this.state.passwordErr && (
                    <ErrMsg errMsg={'Please provide the password'} />
                  )}
                </View>
                <View>
                  {this.state.err && (
                    <View style={{alignSelf: 'center', marginTop: hp('2%')}}>
                      <ErrMsg errMsg={this.state.errMsg} />
                    </View>
                  )}
                  <Button
                    height="7%"
                    bColor={colors.red}
                    width="100%"
                    mTop="4%"
                    activityIndicator={this.state.activityIndicator}
                    textSize={24}
                    text="Login"
                    onPress={() => this.submit()}
                  />
                  {/* <Text
                    style={[
                      styles.greyText,
                      {
                        alignSelf: 'center',
                        marginTop: hp('3%'),
                      },
                    ]}>
                    Or Login with Social media
                  </Text> */}
                </View>
              </View>
              <View style={styles.socialButtons}>
                <View style={{flex: 1}}>
                  {/* <Button
                    height="7%"
                    bColor={colors.fbBlue}
                    width="90%"
                    textSize={16}
                    mTop="3%"
                    firsttextSize={20}
                    firsttext="f"
                    text="facebook"
                    activityIndicator={this.state.activityIndicatorSocialFb}
                    onPress={() => this.fbLogin()}
                    // func={() => this.loginWithGoogleFunc()}
                  /> */}
                </View>
                <View style={{flex: 1}}>
                  {/* <Button
                    height="7%"
                    bColor={colors.googlePink}
                    width="90%"
                    textSize={16}
                    mTop="3%"
                    firsttextSize={20}
                    firsttext="G"
                    text="google"
                    activityIndicator={this.state.activityIndicatorSocial}
                    // func={() => this.loginWithGoogleFunc()}
                    onPress={() => this.loginWithGoogleFunc()}
                  /> */}
                </View>
              </View>
              <View>
                {/* {Platform.OS === 'ios' && (
                  <View style={{alignSelf: 'center'}}>
                    {SignInWithAppleButton(styles.appleBtn, this.appleSignIn)}
                  </View>
                )} */}
                <TouchableOpacity
                  onPress={() => this.goToScreen('ForgotPassword')}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontSize: 14,
                      marginTop: hp('3%'),
                      color: '#7194FE',
                    }}>
                    Forgot Password?
                  </Text>
                </TouchableOpacity>
                {/* 
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginVertical: hp('3%'),
                  }}>
                  <Text
                    style={{
                      fontSize: 18,
                      color: '#cccccc',
                    }}>
                    Don't have an account?
                  </Text>
                  <TouchableOpacity onPress={() => this.goToScreen('Signup')}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: '#FF483D',
                        marginLeft: 5,
                        textDecorationLine: 'underline',
                      }}>
                      Register
                    </Text>
                  </TouchableOpacity>
                </View> */}
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical:hp('5%')
  },
  mainContainer: {
    flex: 4,
  },
  innerContainer: {
    width: wp('80%'),
    marginBottom: hp('5%'),
    marginLeft: wp('10%'),
  },
  socialButtons: {
    flexDirection: 'row',
  },
  greyText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#cccccc',
    marginTop: hp('1%'),
  },
  appleBtn: {height: 44, width: 200},
});
const inputStyles1 = StyleSheet.create({
  input: {
    fontSize: 18,
    borderBottomWidth: 2,
    width: wp('85%'),
    borderBottomColor: 'grey',
    marginTop: Platform.OS == 'ios' ? hp('1.5%') : hp('0%'),
    padding: Platform.OS == 'ios' ? 10 : 10,
  },
});

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
  };
};

export default connect(
  mapStateToProps,
  {...loginAction},
)(Login);
