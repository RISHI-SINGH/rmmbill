import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TextInput,
  BackHandler,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {connect} from 'react-redux';

import * as verifyForgotPasswordOTPAction from '../action/verifyForgotPasswordOTPAction';
import Header from '../component/Header';
import Button from '../component/Button';
import ErrMsg from '../component/ErrMsg';
import {images} from '../utils';

const BACKGROUND_IMAGE = images.backBannerOne;

class VerifyOTP extends Component {
  state = {
    otpSent: null,
    otp: '',
    error: false,
    otpApi: '1234',
    errMsg: '',
    userToken: '',
    activityIndicator: false,
    userData: [],
    keyword: '',
    data: '',
  };

  componentDidMount = async () => {
    const {navigation} = this.props;
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () =>
      navigation.navigate('ForgotPassword'),
    );
    console.warn('123457-->', this.props);
    this.setState({keyword: this.props.route.params.keyword});
  };

  componentDidUpdate = async (prevProps, prevState) => {
    if (
      prevProps.verifyForgotPasswordOTPReducer !==
      this.props.verifyForgotPasswordOTPReducer
    ) {
      const {status, message, data} = this.props.verifyForgotPasswordOTPReducer;
      this.setState({data: data, activityIndicator: false});
      if (status === 200) {
        Alert.alert(
          'mBill',
          message,
          [
            {
              text: 'OK',
              onPress: () => {
                this.props.navigation.navigate('ResetPassword');
              },
            },
            {
              text: 'Cancel',
              style: 'cancel',
            },
          ],
          {cancelable: false},
        );
      } else {
        this.setState({
          error: true,
          errMsg: 'Please check OTP',
          activityIndicator: false,
        });
      }
    }
  };

  componentWillUnmount() {
    this.backHandler.remove();
  }

  checkOtp = () => {
    let {otp, error, errMsg, activityIndicator, keyword} = this.state;
    if (otp === '') {
      error = true;
      errMsg = 'Please provide an OTP';
    } else {
      error = false;
      errMsg = '';
      activityIndicator = true;
      let data = {otp: otp, keyword: keyword};
      this.props.verifyForgotPasswordOTPAction(data);
    }
    this.setState({
      error,
      errMsg,
      activityIndicator,
    });
  };

  render() {
    console.warn('12345678--->', this.props.verifyForgotPasswordOTPReducer);

    return (
      <View style={styles.container}>
        <ImageBackground style={styles.imgBackground} source={BACKGROUND_IMAGE}>
          <View style={styles.headerContainer}>
            <Header headingName="OTP" headingColor="white" {...this.props} />
          </View>
        </ImageBackground>
        <View style={styles.body}>
          <View style={{flex: 6}}>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Enter OTP"
                keyboardType="number-pad"
                style={{marginLeft: wp('5%'), fontSize: 20}}
                onChangeText={value => this.setState({otp: value})}
                returnKeyType="done"
                keyboardType="number-pad"
              />
            </View>
            {this.state.error && (
              <View style={{marginTop: hp('2%'), alignSelf: 'center'}}>
                <ErrMsg errMsg={this.state.errMsg} />
              </View>
            )}
            <Button
              height="7%"
              bColor="red"
              width="80%"
              mTop="2%"
              activityIndicator={this.state.activityIndicator}
              textSize={24}
              text="Submit"
              onPress={() => this.checkOtp()}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  imgBackground: {
    height: hp('25%'),
  },
  headerContainer: {
    height: hp('10%'),
  },
  body: {
    flex: 6,
  },
  inputContainer: {
    width: wp('80%'),
    borderColor: 'red',
    borderWidth: 3,
    height: hp('9%'),
    marginTop: hp('5%'),
    justifyContent: 'center',
    borderRadius: 50,
    alignSelf: 'center',
  },
  msgContainer: {
    flex: 1,
    width: wp('80%'),
    alignSelf: 'center',
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {fontSize: 18, color: 'grey'},
});

const mapStateToProps = state => {
  return {
    verifyForgotPasswordOTPReducer: state.verifyForgotPasswordOTPReducer,
  };
};

export default connect(
  mapStateToProps,
  {...verifyForgotPasswordOTPAction},
)(VerifyOTP);
