import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CalendarStrip from 'react-native-calendar-strip';

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const markedDatesArray = [
      {
       
        date: '2020-07-25',
        lines: [
          {
            color: 'green',
          },
        ],
      },
    ];

    return (
      <View style={styles.container}>
        <CalendarStrip
          style={{height: 150, paddingTop: 20, paddingBottom: 10}}
          calendarAnimation={{type: 'sequence', duration: 40}}
          markedDates={markedDatesArray}
        />
      </View>
    );
  }
}

export default Test;
// import { View, StyleSheet } from 'react-native';
// import CalendarStrip from 'react-native-calendar-strip';

// const Test = () => (
//   <View style={styles.container}>
//     <CalendarStrip
//       style={{height:150, paddingTop: 20, paddingBottom: 10}}
//     />
//   </View>
// );

const styles = StyleSheet.create({
  container: {flex: 1},
});
