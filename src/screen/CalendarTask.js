import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Alert,
  Image,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Header from '../component/Header';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import DatePicker from 'react-native-datepicker';
import * as getTicketCalendarAction from '../action/getTicketCalendarAction';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import RNPickerSelect from 'react-native-picker-select';
import CalendarStrip from 'react-native-calendar-strip';
import {images} from '../utils';

const {phone, location} = images;

class CalendarTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: {
        '2020-07-21': {
          color: '#ED4F34',
          textColor: 'white',
        },
      },
      startDate: '',
      endDate: '',
      listData: [],
      loader: true,
      token: '',
      status: '',
      month: [{label: 'Month', value: '0'}, {label: 'Week', value: '1'}],
      nextDays: [],
      selectedValue: '0',
    };
  }
  componentDidMount = async () => {
    const token = await AsyncStorage.getItem('@user_token');
    this.setState({token});
    this.setState({loader: true, status: this.props.route.params.status});
    let today = new Date();

    let date1 =
      today.getFullYear() +
      '-' +
      parseInt(today.getMonth() + 1) +
      '-' +
      today.getDate();

    let date2 = today.getFullYear() + '-' + '01' + '-' + '01';
    const minDate = moment(date1, 'YYYY-MM-DD').format();
    const maxDate = moment(date2, 'YYYY-MM-DD').format();
    const d1 = minDate.split('T')[0];
    const d2 = maxDate.split('T')[0];
    this.setState({startDate: d2, endDate: d1});
    const data = {
      fromDate: this.state.startDate,
      toDate: this.state.endDate,
    };
    console.warn('hello-->', data);
    this.props.getTicketCalendarAction(data, token);
  };

  componentDidUpdate = PrevProps => {
    if (
      PrevProps.getTicketCalendarReducer != this.props.getTicketCalendarReducer
    ) {
      const {
        data,
        status,
        message,
        error,
      } = this.props.getTicketCalendarReducer;
      if (status === 200) {
        this.setState({
          listData: data,
          loader: false,
        });
        data.map(item => {
          console.warn('currentstatusRM-->', item.currentStatusRm);
        });
        let result = data.map(a =>
          a.currentStatusRm === this.state.status ? a.rmExpiryDate : undefined,
        );
        console.warn('result-->', result);
        this.setState({nextDays: result});

        let markedDatesArray = [];
        data.map((item, index) => {
          if (item.currentStatusRm === this.state.status) {
            let markedDates = {
              date: item.rmExpiryDate,
              lines: [
                {
                  color: '#ED4F34',
                },
              ],
            };
            markedDatesArray.push(markedDates);
          }
        });
        this.setState({markedDatesArray});

        // data.map(item => {
        //   if (item.rmExpiryDate && item.currentStatusRm === 1) {
        //     let expDate = new Date(item.rmExpiryDate);
        //     console.warn('hellodate-->', item.ticketNumber);

        //     let today = new Date();

        //     if (expDate < today) {
        //       const data = {
        //         ticketNumber: item.ticketNumber,
        //       };
        //       console.warn('ChangeStatusRequest--->', data);
        //       this.props.changeStatusAction(data, this.state.token);
        //     }
        //   }
        // });
      } else {
        this.setState({
          listData: [],
          loader: false,
          nextDays: [],
        });
      }
    }
  };

  setFields = (key, value) => {
    console.warn(key, value);
    const err = key + 'Err';
    this.setState({
      [key]: value,
    });
  };

  startDate = date => {
    this.setState({startDate: date}, () => {
      Alert.alert('Please Select toDate.');
      this.setState({endDate: ''});
    });
  };
  endDate = date => {
    this.setState({endDate: date}, () => {
      const data = {
        fromDate: this.state.startDate,
        toDate: this.state.endDate,
      };
      console.warn('datess-->', data);
      this.props.getTicketCalendarAction(data, this.state.token);
    });
  };
  setPicker = value => {
    this.setState({selectedValue: value});
  };

  render() {
    console.warn('nextDays-->', this.state.nextDays);
    let mark = {};

    // const markedDatesArray = [
    //   {
    //     date: '2020-07-25',
    //     lines: [
    //       {
    //         color: '#ED4F34',
    //       },
    //     ],
    //   },
    // ];

    this.state.nextDays.forEach(day => {
      console.warn('hello', mark[day]);
      mark[day] = {
        customStyles: {
          container: {
            backgroundColor: '#ED4F34',
          },
          text: {
            color: '#fff',
            fontWeight: 'bold',
          },
        },
      };
    });
    return (
      <View style={{flex: 1}}>
        <View style={{height: hp('10%')}}>
          <Header
            leftIcon="chevronLeft"
            headingName="Calendar"
            {...this.props}
          />
        </View>

        <View style={{flex: 10.5}}>
          <ScrollView>
            <View
              style={{
                height: hp('10%'),
                width: wp('90%'),
                alignSelf: 'center',
              }}>
              <View
                style={{
                  height: hp('4%'),
                  flexDirection: 'row',
                  alignSelf: 'center',
                  width: wp('90%'),
                }}>
                <View
                  style={{
                    height: hp('4%'),
                    width: wp('30%'),
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: 12}}>From Date</Text>
                </View>
                <View
                  style={{
                    height: hp('4%'),
                    width: wp('30%'),
                    justifyContent: 'center',
                  }}>
                  <Text style={{fontSize: 12}}>To Date</Text>
                </View>
                <View
                  style={{
                    height: hp('4%'),
                    width: wp('30%'),
                    justifyContent: 'center',

                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: 12}}>View Tasks</Text>
                </View>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('30%'),
                  }}>
                  <View
                    style={{
                      height: hp('5%'),
                      width: wp('45%'),
                      flexDirection: 'row',
                    }}>
                    <DatePicker
                      showIcon={false}
                      style={{width: wp('45%')}}
                      date={this.state.startDate}
                      mode="date"
                      placeholder="YYYY-MM-DD"
                      format="YYYY-MM-DD"
                      //maxDate={this.getDate()}
                      // minDate={new Date(Date.now()+(10 * 60 * 1000))}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 0,
                          marginLeft: wp('22%'),
                        },
                        dateInput: {
                          marginRight: wp('20%'),
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={date => {
                        // this.setState({startDate: date});
                        this.startDate(date);
                      }}
                    />
                  </View>
                </View>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('30%'),
                  }}>
                  <View
                    style={{
                      height: hp('4%'),
                      width: wp('45%'),
                      flexDirection: 'row',
                    }}>
                    <DatePicker
                      showIcon={false}
                      style={{width: wp('45%')}}
                      date={this.state.endDate}
                      mode="date"
                      placeholder="YYYY-MM-DD"
                      format="YYYY-MM-DD"
                      //maxDate={this.getDate()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 0,
                          marginLeft: wp('22%'),
                        },
                        dateInput: {
                          marginRight: wp('20%'),
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={date => this.endDate(date)}
                    />
                  </View>
                </View>
                <View
                  style={{
                    height: hp('5.2%'),
                    width: wp('30%'),
                    borderWidth: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RNPickerSelect
                    placeholder={{}}
                    value={this.state.selectedValue}
                    onValueChange={value => this.setPicker(value)}
                    items={this.state.month}
                  />
                </View>
              </View>
            </View>
            {this.state.selectedValue == '0' ? (
              <Calendar
                markingType={'custom'}
                markedDates={mark}
                //current={'2012-03-01'}
              />
            ) : (
              <CalendarStrip
                style={{height: 150, paddingTop: 20, paddingBottom: 10}}
                calendarAnimation={{type: 'sequence', duration: 40}}
                markedDates={this.state.markedDatesArray}
                scrollable={true}
              />
            )}

            <View
              style={{
                height: hp('4%'),
                backgroundColor: 'grey',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  color: '#fff',
                  fontWeight: '700',
                  marginLeft: wp('5%'),
                }}>
                Tasks
              </Text>
            </View>
            <View>
              {this.state.listData.length === 0 ? (
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: hp('5%'),
                  }}>
                  <Text style={{fontSize: 16}}>No Tickets found</Text>
                </View>
              ) : this.state.listData.length !== 0 ? (
                this.state.listData.map((item, index) => {
                  if (item.currentStatusRm === this.state.status) {
                    return (
                      <View
                        style={{
                          height: hp('10%'),
                          backgroundColor: '#ffffff',
                          marginTop: hp('0.5%'),
                          marginBottom: hp('1%'),
                          width: wp('95%'),
                          alignSelf: 'center',
                          borderRadius: 15,
                          elevation: 5,
                          borderWidth: 1,
                          flexDirection: 'row',
                        }}>
                        <View
                          style={{
                            height: hp('9.7%'),
                            width: wp('8%'),
                            backgroundColor: '#ED4F34',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderTopLeftRadius: 14,
                            borderBottomLeftRadius: 14,
                          }}>
                          <Text
                            style={{
                              fontSize: 16,
                              color: '#ffffff',
                              fontWeight: 'bold',
                            }}>
                            {item.rmExpiryDate.split('-')[2]}
                          </Text>
                          <Text
                            style={{
                              fontSize: 12,
                              color: '#ffffff',
                            }}>
                            {moment(item.rmExpiryDate)
                              .format('MMM')
                              .toUpperCase()}
                          </Text>
                        </View>
                        <View style={{height: hp('10%'), width: wp('50%')}}>
                          {this.state.selectedValue == '0' ? (
                            <View
                              style={{
                                height: hp('4%'),
                                width: wp('50%'),
                                justifyContent: 'center',
                                marginLeft: wp('1%'),
                              }}>
                              <Text
                                style={{
                                  fontSize: 16,
                                  fontWeight: 'bold',
                                }}>
                                Type: {item.ticketType}
                              </Text>
                            </View>
                          ) : (
                            <View
                              style={{
                                height: hp('4%'),
                                width: wp('50%'),
                                justifyContent: 'center',
                                marginLeft: wp('1%'),
                              }}>
                              <Text
                                style={{
                                  fontSize: 16,
                                  fontWeight: 'bold',
                                }}>
                                {item.contactPersonName}
                              </Text>
                            </View>
                          )}
                          {this.state.selectedValue == '0' ? (
                            <View
                              style={{
                                height: hp('3%'),
                                width: wp('50%'),

                                flexDirection: 'row',
                                marginLeft: wp('1%'),
                              }}>
                              <Text
                                style={{
                                  fontSize: 12,

                                  alignSelf: 'center',
                                }}>
                                City :
                              </Text>
                              <Text style={{fontSize: 12, alignSelf: 'center'}}>
                                {item.City}
                              </Text>
                            </View>
                          ) : (
                            <View
                              style={{
                                height: hp('3%'),
                                width: wp('50%'),

                                flexDirection: 'row',
                                marginLeft: wp('1%'),
                              }}>
                              <Image
                                source={location}
                                style={{
                                  height: 12,
                                  width: 12,
                                  alignSelf: 'center',
                                }}
                              />
                              <Text
                                style={{
                                  fontSize: 12,
                                  alignSelf: 'center',
                                  marginLeft: wp('1%'),
                                }}>
                                {item.STATE}
                              </Text>
                            </View>
                          )}
                          {this.state.selectedValue == '0' ? (
                            <View
                              style={{
                                height: hp('3%'),
                                width: wp('50%'),

                                flexDirection: 'row',
                                marginLeft: wp('1%'),
                              }}>
                              <Text style={{fontSize: 12}}>User Code :</Text>
                              <Text style={{fontSize: 12}}>{item.rmCode}</Text>
                              <Text
                                style={{fontSize: 12, marginLeft: wp('2%')}}>
                                RM :
                              </Text>
                              <Text
                                style={{fontSize: 12, marginLeft: wp('2%')}}>
                                {item.rmId}
                              </Text>
                            </View>
                          ) : (
                            <View
                              style={{
                                height: hp('3%'),
                                width: wp('50%'),

                                flexDirection: 'row',
                                marginLeft: wp('1%'),
                              }}>
                              <Text
                                style={{
                                  fontSize: 12,
                                  alignSelf: 'center',
                                }}>
                                Phone No.:{' '}
                              </Text>
                              <Image
                                source={phone}
                                style={{
                                  height: 12,
                                  width: 12,
                                  alignSelf: 'center',
                                }}
                              />
                              <Text
                                style={{
                                  fontSize: 12,
                                  alignSelf: 'center',
                                  marginLeft: wp('1%'),
                                  color: '#ED4F34',
                                }}>
                                {item.registedMobileNumber}
                              </Text>
                            </View>
                          )}
                        </View>
                        <View
                          style={{
                            height: hp('10%'),
                            width: wp('40%'),
                          }}>
                          <View
                            style={{
                              height: hp('6%'),
                              width: wp('40%'),
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: 16,
                                fontWeight: 'bold',
                                color: 'red',
                              }}>
                              {item.rmExpiryDate}
                            </Text>
                            {/* <Text>{moment(item.rmExpiryDate).format('dddd')}</Text> */}
                          </View>
                          <View
                            style={{
                              height: hp('4%'),
                              width: wp('40%'),
                              alignItems: 'center',
                            }}>
                            <Text style={{fontSize: 10}}>Due Date</Text>
                          </View>
                        </View>
                      </View>
                    );
                  }
                })
              ) : (
                <View
                  style={{
                    height: hp('10%'),
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ActivityIndicator size="large" color="orange" />
                </View>
              )}
            </View>
          </ScrollView>
        </View>
        <View style={{flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Home')}
              style={{
                height: hp('8%'),
                backgroundColor: '#fff',
                width: wp('50%'),
                justifyContent: 'center',
                alignItems: 'center',

                elevation: 10,
              }}>
              <Text
                style={{fontSize: 16, color: '#ED4F34', fontWeight: 'bold'}}>
                TASK LIST{' '}
              </Text>
            </TouchableOpacity>
            <View
              onPress={() => this.props.navigation.navigate('Calendar')}
              style={{
                height: hp('8%'),

                width: wp('50%'),
                justifyContent: 'center',
                alignItems: 'center',

                elevation: 10,
                backgroundColor: '#ED4F34',
              }}>
              <Text style={{fontSize: 16, color: '#fff', fontWeight: 'bold'}}>
                CALENDAR{' '}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    getTicketCalendarReducer: state.getTicketCalendarReducer,
    // getStateReducer: state.getStateReducer,
    // changeStatusReducer: state.changeStatusReducer,
  };
};

export default connect(
  mapStateToProps,
  {...getTicketCalendarAction},
)(CalendarTask);
