import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  PermissionsAndroid,
  ScrollView,
  Alert,
  ActivityIndicator,
  Linking,
  TouchableWithoutFeedback,
} from 'react-native';
import Header from '../component/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {connect} from 'react-redux';
import {images, alert} from '../utils';
import RNPickerSelect from 'react-native-picker-select';
import * as dispositionAction from '../action/dispositionAction';
import AsyncStorage from '@react-native-community/async-storage';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import Geolocation from 'react-native-geolocation-service';
import * as postTicket from '../action/postTicket';
import * as visitCount from '../action/visitCount';
import moment from 'moment';
import Modal from 'react-native-modal';
import {WebView} from 'react-native-webview';

const {phone, location} = images;
import * as dummyApiAction from '../action/dummyApiAction';
import {TextInput} from 'react-native-gesture-handler';
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 4;
const options = ['Cancel', 'Camera'];
const title = 'Open Image from';
class TaskDetail4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dispositionData: [],
      dispositionId: '',
      ProfileImages: '',
      baseImage: undefined,
      taskData: '',
      currentLatitude: '',
      currentLongitude: '',
      visitCount: '',
      Remark: undefined,
      date: undefined,
      initialPosition: '',
      spinner: false,
      isVisible: false,
    };
  }
  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }

  async componentDidMount() {
    let tomorrow = moment()
      .add(1, 'days')
      .format('YYYY-MM-DD');
    this.setState({date: tomorrow});

    Geolocation.getCurrentPosition(
      position => {
        const lat = JSON.stringify(position.coords.latitude);
        const lon = JSON.stringify(position.coords.longitude);
        this.setState({currentLatitude: lat, currentLongitude: lon});
      },
      error => Alert.alert('Error', JSON.stringify(error)),
      {
        enableHighAccuracy: true,
        timeout: 20000000000000,
        maximumAge: 10000000000000,
      },
    );
    this.watchID = Geolocation.watchPosition(position => {
      const lastPosition = JSON.stringify(position);
      this.setState({lastPosition});
    });

    //   if (Platform.OS === 'ios') {
    //     this.getCurrentLocation();
    //   } else {
    //     try {
    //       const granted = await PermissionsAndroid.request(
    //         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    //         {
    //           title: 'Device current location permission',
    //           message: 'Allow app to get your current location',

    //           buttonPositive: 'OK',
    //         },
    //       );
    //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //         this.getCurrentLocation();
    //       } else {
    //         console.warn('Location permission denied');
    //       }
    //     } catch (err) {
    //       console.warn(err);
    //     }
    //  }
    const token = await AsyncStorage.getItem('@user_token');
    this.setState({token});

    this.setState({
      taskData:
        this.props.route &&
        this.props.route.params &&
        this.props.route.params.data,
    });

    if (this.state.taskData.currentStatusRm !== 5) {
      const data = {
        status: this.state.taskData.currentStatusRm,
        ticketType: this.state.taskData.ticketType,
      };

      this.props.dispositionAction(data, token);
    }

    this.props.visitCount(token);

    // let dueDate = new Date(this.state.taskData.ticketDate);

    // let today = new Date();

    // if (dueDate < today) {
    //   alert('Working!');
    // }
  }

  // getCurrentLocation() {
  //   Geolocation.requestAuthorization();
  //   Geolocation.getCurrentPosition(
  //     position => {
  //       console.warn('current Loc Taskdetails-->', position);
  //       this.setState({
  //         currentLongitude: position.coords.longitude,
  //         currentLatitude: position.coords.latitude,
  //       });
  //     },
  //     error => {
  //       console.warn('map error: ', error);
  //       console.warn(error.code, error.message);
  //     },
  //     {enableHighAccuracy: false, timeout: 15000, maximumAge: 10000},
  //   );
  // }

  submitHandle = () => {
    const {
      taskData,
      Remark,
      baseImage,
      currentLatitude,
      currentLongitude,
      dispositionId,
      date,
      visitCount,
    } = this.state;
    if (dispositionId === '') {
      Alert.alert('Please select the disposition.');
    } else {
      const data = {
        ticketNumber: taskData.ticketNumber,
        rmRemark: Remark,
        dispositionId: dispositionId,
        // ticketImage: baseImage,
        location: {lat: currentLatitude, lon: currentLongitude},
        scheduleDate:
          (dispositionId === 19 || dispositionId === 26) &&
          taskData.successfulVisit >= visitCount
            ? undefined
            : date,
      };
      console.warn('submit---data-->', data);
      this.setState({spinner: true});
      this.props.postTicket(data, this.state.token);
    }
  };

  componentDidUpdate(PrevProps) {
    if (PrevProps.dispositionReducer != this.props.dispositionReducer) {
      const {data, status, messgae, error} = this.props.dispositionReducer;
      if (status === 200) {
        let DATA = [];
        if (data) {
          data.map((item, index) => {
            const obj = {};
            obj.label = item.dispositionName;
            obj.value = item.dispositionId;
            DATA = [...DATA, obj];
          });
        }
        this.setState({dispositionData: DATA});
      } else {
        Alert.alert(
          'mBill',
          error,
          [
            {
              text: 'OK',
              //onPress: () => alert('ok'),
            },
          ],
          {cancelable: false},
        );
      }
    }
    if (PrevProps.visitCountReducer != this.props.visitCountReducer) {
      const {data, status, error} = this.props.visitCountReducer;
      if (status === 200) {
        this.setState({visitCount: data});
      } else {
        Alert.alert(
          'mBill',
          error,
          [
            {
              text: 'OK',
              //onPress: () => alert('ok'),
            },
          ],
          {cancelable: false},
        );
      }
    }
    if (PrevProps.postTicketReducer != this.props.postTicketReducer) {
      const {data, status, error, message} = this.props.postTicketReducer;
      if (status === 200) {
        this.setState({spinner: false});
        Alert.alert(
          'mBill',
          message,
          [
            {
              text: 'OK',
              onPress: () => this.props.navigation.navigate('Home'),
            },
          ],
          {cancelable: false},
        );
      } else {
        this.setState({spinner: false});
        Alert.alert(
          'mBill',
          error,
          [
            {
              text: 'OK',
              //onPress: () => alert('ok'),
            },
          ],
          {cancelable: false},
        );
      }
    }
  }

  setFields = (key, value) => {
    const err = key + 'Err';
    this.setState({
      [key]: value,
      [err]: false,
    });
  };

  async handlePress(i) {
    console.warn('ißßßßßß', i);
    // this.setState({
    //   selected: i,
    // });
    if (i == 1) {
      ImagePicker.openCamera({
        width: 300,
        height: 300,
        compressImageQuality: 0.5,
        cropping: true,
        multiple: false,
        includeBase64: true,
      })
        .then(image => {
          this.setState({ProfileImages: image, baseImage: image.data}, () => {
            console.warn('image 1', image);
          });
        })
        .catch(err => {
          console.warn(err);
        });
    } else if (i == 2) {
      ImagePicker.openPicker({
        width: 300,
        height: 300,
        compressImageQuality: 0.5,
        cropping: true,
        multiple: false,
        includeBase64: true,
      })
        .then(image => {
          // console.log("---------->", image);

          this.setState({ProfileImages: image, baseImage: image.data}, () => {
            console.warn('images 2', image);
          });
        })
        .catch(err => {
          alert(err.message);
        });
    }
  }

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  getDate = () => {
    const date = new Date();
    let currentMonth = date.getMonth();
    let currentDay = date.getDate();
    currentMonth =
      currentMonth + 1 < 9 ? '0' + (currentMonth + 1) : currentMonth + 1;
    currentDay = currentDay < 10 ? '0' + currentDay : currentDay;
    const today = date.getFullYear() + '-' + currentMonth + '-' + currentDay;
    return today;
  };

  viewInvoices = () => {
    // if (this.state.Remark === '') {
    //   Alert.alert('mBill', 'Please enter the remark.', [
    //     {
    //       text: 'OK',
    //     },
    //   ]);
    // } else
    // {
    //   this.props.navigation.navigate('ViewInvoice', {
    //     ticketNumber: this.state.taskData.ticketNumber,
    //     dispositionId: this.state.dispositionId,
    //     lat: this.state.currentLatitude,
    //     long: this.state.currentLongitude,
    //     remark: this.state.Remark,
    //   });
    // }

    this.props.navigation.navigate('ViewInvoice', {
      ticketNumber: this.state.taskData.ticketNumber,
      dispositionId: this.state.dispositionId,
      lat: this.state.currentLatitude,
      long: this.state.currentLongitude,
      remark: this.state.Remark,
    });
  };
  openLocationModal = () => {
    this.setState({isVisible: true});
  };

  dismiss = () => {
    this.setState({isVisible: false});
  };

  render() {
    console.warn(
      'status-->',
      this.state.taskData.successfulVisit + 1,
      this.state.visitCount,
    );
    console.warn('visitCount-->', this.state.visitCount);

    const {taskData} = this.state;
    console.warn('data-->', taskData);
    let bool;

    if (
      (this.state.dispositionId === 19 || this.state.dispositionId === 26) &&
      taskData.ticketType == 'onBoarding' &&
      taskData.successfulVisit + 1 < this.state.visitCount
    ) {
      console.warn('m here');
      bool = true;
    } else {
      console.warn('m here notttttt');
      bool = false;
    }
    return (
      <View style={{flex: 1}}>
        <View style={{height: hp('10%')}}>
          <Header
            leftIcon="chevronLeft"
            headingName="Task Detail"
            {...this.props}
          />
        </View>
        <KeyboardAwareScrollView>
          <View
            style={{
              width: wp('90%'),
              alignSelf: 'center',
              borderRadius: 10,
              borderWidth: 1,
            }}>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Name
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 16,

                    color: 'grey',
                  }}>
                  {taskData.contactPersonName}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Address
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 14,

                    color: 'grey',
                  }}
                  numberOfLines={2}>
                  {taskData.addressLine1 == null
                    ? 'N/A'
                    : taskData.addressLine1}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 16,

                    color: 'grey',
                  }}
                />
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 14,

                    color: 'grey',
                  }}>
                  {taskData.STATE}, {taskData.City}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Location
                </Text>
              </View>
              <TouchableOpacity
                onPress={this.openLocationModal}
                style={{
                  height: hp('6%'),
                  width: wp('10%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Image source={location} style={{height: 30, width: 30}} />
              </TouchableOpacity>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('30%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 14,

                    color: 'grey',
                  }}
                  numberOfLines={2}>
                  {taskData.addressLine1 == null
                    ? 'N/A'
                    : taskData.addressLine1}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Phone
                </Text>
              </View>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(`tel:${taskData.registedMobileNumber}`)
                }
                style={{
                  height: hp('6%'),
                  width: wp('10%'),
                  borderBottomWidth: 2.5,
                  justifyContent: 'center',

                  borderBottomColor: 'grey',
                }}>
                <Image source={phone} style={{height: 30, width: 30}} />
              </TouchableOpacity>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('30%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 16,

                    color: 'grey',
                  }}>
                  {taskData.registedMobileNumber}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Type
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,

                  justifyContent: 'center',
                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 16,

                    color: 'grey',
                  }}>
                  {taskData.ticketType}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  User Code
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,
                  justifyContent: 'center',

                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 16,

                    color: 'grey',
                  }}>
                  {taskData.rmCode}
                </Text>
              </View>
            </View>
            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  RM
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,
                  justifyContent: 'center',

                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 16,

                    color: 'grey',
                  }}>
                  {taskData.rmId}
                </Text>
              </View>
            </View>

            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Ticket No.
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,
                  justifyContent: 'center',

                  borderBottomColor: 'grey',
                }}>
                <Text
                  style={{
                    marginLeft: wp('1%'),
                    fontSize: 16,

                    color: 'grey',
                  }}>
                  {taskData.ticketNumber}
                </Text>
              </View>
            </View>
            {this.state.taskData.currentStatusRm !== 5 ? (
              <View
                style={{
                  height: hp('6%'),
                  width: wp('90%'),
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('45%'),
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      marginLeft: wp('8%'),
                      fontSize: 18,
                      fontWeight: '500',
                      color: 'grey',
                    }}>
                    Status
                  </Text>
                </View>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('40%'),
                    borderBottomWidth: 2.5,
                    justifyContent: 'center',

                    borderBottomColor: 'grey',
                  }}>
                  <RNPickerSelect
                    style={genderPicker}
                    onValueChange={value =>
                      this.setFields('dispositionId', value)
                    }
                    items={this.state.dispositionData}
                  />
                </View>
              </View>
            ) : null}

            <View
              style={{
                height: hp('6%'),
                width: wp('90%'),
                flexDirection: 'row',
              }}>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('45%'),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    marginLeft: wp('8%'),
                    fontSize: 18,
                    fontWeight: '500',
                    color: 'grey',
                  }}>
                  Remark
                </Text>
              </View>
              <View
                style={{
                  height: hp('6%'),
                  width: wp('40%'),
                  borderBottomWidth: 2.5,
                  justifyContent: 'center',

                  borderBottomColor: 'grey',
                }}>
                <TextInput
                  placeholder="Add Remark"
                  style={{fontSize: 16}}
                  onChangeText={text => this.setState({Remark: text})}
                />
              </View>
            </View>

            {bool === true ? (
              <View
                style={{
                  height: hp('6%'),
                  width: wp('90%'),
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('45%'),
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      marginLeft: wp('8%'),
                      fontSize: 18,
                      fontWeight: '500',
                      color: 'grey',
                    }}>
                    Select Date
                  </Text>
                </View>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('40%'),

                    justifyContent: 'center',
                  }}>
                  <DatePicker
                    style={{width: wp('40%'), height: hp('5%')}}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate={this.state.date}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        marginLeft: 0,
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={date => {
                      this.setState({date: date});
                    }}
                  />
                </View>
              </View>
            ) : this.state.dispositionId === 19 ||
              this.state.dispositionId === 26 ? null : (
              <View
                style={{
                  height: hp('6%'),
                  width: wp('90%'),
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('45%'),
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      marginLeft: wp('8%'),
                      fontSize: 18,
                      fontWeight: '500',
                      color: 'grey',
                    }}>
                    Select Date
                  </Text>
                </View>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('40%'),

                    justifyContent: 'center',
                  }}>
                  <DatePicker
                    style={{width: wp('40%'), height: hp('5%')}}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate={this.state.date}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        marginLeft: 0,
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={date => {
                      this.setState({date: date});
                    }}
                  />
                </View>
              </View>
            )}

            {/* {this.state.dispositionId === 19 &&
            taskData.successfulVisit >= this.state.visitCount ? (
              <View>
                <Text>hello</Text>
              </View>
            ) : (
              <View
                style={{
                  height: hp('6%'),
                  width: wp('90%'),
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('45%'),
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      marginLeft: wp('8%'),
                      fontSize: 18,
                      fontWeight: '500',
                      color: 'grey',
                    }}>
                    Select Date
                  </Text>
                </View>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('40%'),

                    justifyContent: 'center',
                  }}>
                  <DatePicker
                    style={{width: wp('40%'), height: hp('5%')}}
                    date={this.state.date}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate={this.state.date}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                      },
                      dateInput: {
                        marginLeft: 0,
                      },
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={date => {
                      this.setState({date: date});
                    }}
                  />
                </View>
              </View>
            )} */}
            {/* {this.state.dispositionId === 18 ? (
              <View
                style={{
                  height: hp('6%'),
                  width: wp('90%'),
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('45%'),
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      marginLeft: wp('8%'),
                      fontSize: 18,
                      fontWeight: '500',
                      color: 'grey',
                    }}>
                    Select Image
                  </Text>
                </View>

                <ActionSheet
                  ref={o => (this.ActionSheet = o)}
                  title={title}
                  options={options}
                  cancelButtonIndex={CANCEL_INDEX}
                  destructiveButtonIndex={DESTRUCTIVE_INDEX}
                  onPress={index => this.handlePress(index)}
                />
                <View
                  style={{
                    height: hp('6%'),
                    width: wp('40%'),

                    justifyContent: 'center',
                  }}>
                  {this.state.baseImage != undefined ? (
                    <TouchableOpacity
                      onPress={this.showActionSheet}
                      style={{
                        height: hp('4%'),
                        width: wp('40%'),

                        justifyContent: 'center',

                        borderRadius: 20,
                        borderWidth: 1,
                      }}>
                      <Text style={{alignSelf: 'center'}}>
                        {this.state.ProfileImages.mime}
                      </Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={this.showActionSheet}
                      style={{
                        height: hp('4%'),
                        width: wp('40%'),

                        justifyContent: 'center',

                        borderRadius: 20,
                        borderWidth: 1,
                      }}>
                      <Text style={{alignSelf: 'center'}}>Tap to Select</Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            ) : null} */}
          </View>
          <View style={{height: hp('15%')}} />
          {/* 
          {this.state.dispositionId === 19 ? (
            <View
              style={{
                height: hp('10%'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => this.viewInvoices()}
                // onPress={() => {
                //   this.props.navigation.navigate('ViewInvoice', {
                //     ticketNumber: this.state.taskData.ticketNumber,
                //     dispositionId: this.state.dispositionId,
                //     lat: this.state.currentLatitude,
                //     long: this.state.currentLongitude,
                //     remark: this.state.Remark,
                //   });
                // }}
                style={{
                  height: hp('8%'),
                  width: wp('90%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 1,
                  backgroundColor: '#ffffff',
                }}>
                <Text> Tap to View Invoices</Text>
              </TouchableOpacity>
            </View>
          ) : null} */}

          {(this.state.dispositionId === 19 ||
            this.state.dispositionId === 26) &&
          this.state.taskData.ticketType === 'dataQuery' ? (
            <TouchableOpacity
              onPress={this.viewInvoices}
              style={{
                height: hp('7%'),
                width: wp('50%'),
                alignSelf: 'center',
                backgroundColor: '#ED4F34',
                borderRadius: 25,
                marginBottom: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: '#FFFFFF', fontSize: 16, fontWeight: '900'}}>
                View Invoices
              </Text>
            </TouchableOpacity>
          ) : this.state.spinner ? (
            <ActivityIndicator size="large" color="#ED4F34" />
          ) : (
            <TouchableOpacity
              onPress={this.submitHandle}
              style={{
                height: hp('7%'),
                width: wp('50%'),
                alignSelf: 'center',
                backgroundColor: '#ED4F34',
                borderRadius: 25,
                marginBottom: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: '#FFFFFF', fontSize: 16, fontWeight: '900'}}>
                SUBMIT
              </Text>
            </TouchableOpacity>
          )}
        </KeyboardAwareScrollView>
        <Modal
          isVisible={this.state.isVisible}
          onBackButtonPress={this.dismiss}>
          <TouchableWithoutFeedback>
            <View style={styles.modalOverlay} />
          </TouchableWithoutFeedback>
          <View
            style={{
              backgroundColor: 'white',

              borderRadius: 10,
              height: hp('85%'),
            }}>
            <WebView
              source={{
                // uri: 'http://maps.google.com?q=208027',
                uri:
                  'http://maps.google.co.in/maps?q=' +
                  `${taskData.addressLine1},${taskData.pincode}`,
              }}
              style={{marginBottom: hp('5%')}}
              startInLoadingState
            />
            <Text style={{textAlign: 'center'}}>{`${taskData.addressLine1},${
              taskData.STATE
            }${' '}${taskData.pincode}`}</Text>
            <TouchableOpacity
              onPress={() => this.setState({isVisible: false})}
              style={{
                height: hp('5%'),
                backgroundColor: 'grey',

                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 18, fontWeight: 'bold', color: '#fff'}}>
                Close
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const genderPicker = StyleSheet.create({
  placeholder: {
    color: 'grey',
    fontSize: hp('2.3%'),
    width: wp('40%'),
  },
  inputIOS: {
    color: 'grey',
    fontSize: hp('2.3%'),
    paddingVertical: hp('1.5%'),
    paddingLeft: 8,
    width: wp('40%'),
  },
  inputAndroid: {
    color: 'grey',
    fontSize: hp('2.3%'),
    paddingVertical: hp('0.9%'),
    paddingLeft: 8,
    width: wp('40%'),
  },
});

const styles = StyleSheet.create({
  modalOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0)',
  },
});

const mapStateToProps = state => {
  return {
    dispositionReducer: state.dispositionReducer,
    postTicketReducer: state.postTicketReducer,
    visitCountReducer: state.visitCountReducer,
  };
};

export default connect(
  mapStateToProps,
  {...dispositionAction, ...postTicket, ...visitCount},
)(TaskDetail4);
