import { BASE_URL } from '../utils';

export const getStateAction = () => {
  try {
    return async function (dispatch) {
      const data = await fetch(`${BASE_URL}/static/getState`);
      const jsonData = await data.json();
      dispatch({ type: 'GET_STATE', payload: jsonData });
    };
  } catch (error) {
    console.warn('error===>', error);
  }
};
