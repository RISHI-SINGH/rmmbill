import { BASE_URL } from '../utils';

export const verifyForgotPasswordOTPAction = (data) => {
    console.warn("verifyForgotPasswordOTPAction-->>>>>>>>>>>",data)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/user/verifyForgetPasswordOtp`, {
                method: 'POST',
                headers: {
                    "Content-Type":"application/json",
                    
                },
                body:JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            console.warn("modeOFpYAMENT-->>>,",jsonResponse)
            dispatch({ type: 'FORGOT_PASSWORD_OTP', payload: jsonResponse });
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}