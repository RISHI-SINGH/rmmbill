import {BASE_URL} from '../utils';

export function loginAction(userData) {
 console.log("datataLogin-->",userData)
    try {
      return async dispatch => {
        const data = await fetch(`${BASE_URL}/user/login`, {
          method: 'POST',
          headers: {
            "Content-Type":"application/json"  
          },
         
          body: JSON.stringify(userData),
        });
        const jsonData = await data.json();
        dispatch({type: 'CUSTOM', payload: jsonData});
        console.warn('LOGIN-->',jsonData)
      };
    } catch (error) {
      console.warn('error===>', error);
    }
  }

  export function userToken(value){
    return dispatch =>{ dispatch({type:"TOKEN",value:value})}
  
  }