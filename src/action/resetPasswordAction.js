import { BASE_URL } from '../utils';

export const resetPasswordAction = (data,token) => {
    console.warn("data,token-->",data,token)
    try {
       

        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/user/resetPassword`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    "x-auth-token":token
                },
                body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();

            dispatch({ type: 'RESET_PASSWORD', payload: jsonResponse });
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}