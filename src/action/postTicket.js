import { BASE_URL } from '../utils';

export const postTicket = (data,token) => {
     console.warn("datatatata-->",data,token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/postTickets`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                     'x-auth-token': token
                },
                 body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            console.warn("TICKET---->",jsonResponse)
            dispatch({ type: 'TICKET', payload: jsonResponse });
            console.warn("TICKET---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}