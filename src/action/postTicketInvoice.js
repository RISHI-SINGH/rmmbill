import { BASE_URL } from '../utils';

export const postTicketInvoice = (data,token) => {
     console.warn("datatatataInvoice-->",data,token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/postTickets`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                     'x-auth-token': token
                },
                 body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            console.warn("TICKETinvoice---->",jsonResponse)
            dispatch({ type: 'TICKET_INVOICE', payload: jsonResponse });
            console.warn("TICKETINVOICE---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}