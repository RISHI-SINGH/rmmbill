import { BASE_URL } from '../utils';

export const visitCount = (token) => {
     console.warn("datatatata-->",token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/visitNumber`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                },
                // body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            
            dispatch({ type: 'COUNT', payload: jsonResponse });
            console.warn("COUNT---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}