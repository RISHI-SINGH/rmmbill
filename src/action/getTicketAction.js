import { BASE_URL } from '../utils';

export const getTicketAction = (token) => {
    // console.warn("datatatata-->",data,token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/getTickets`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                },
                // body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            
            dispatch({ type: 'GET_TICKET', payload: jsonResponse });
            console.warn("TICKER---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}