import { BASE_URL } from '../utils';

export const changeStatusAction = (data,token) => {
     console.warn("dispotionAction-->",data,token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/changeStatus`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                },
                 body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            
            dispatch({ type: 'CHANGE_STATUS', payload: jsonResponse });
            console.warn("CHANGE_STATUS---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}