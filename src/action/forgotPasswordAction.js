import { BASE_URL } from '../utils';

export const forgotPasswordAction = (data) => {
    try {
       
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/user/forgotPassword`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            dispatch({ type: 'FORGOT_PASSWORD', payload: jsonResponse });
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}