import { BASE_URL } from '../utils';

export const dataQueryAction = (data,token) => {
     console.warn("dataQueryAction-->",data,token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/getDataQuery`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                },
                 body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            
            dispatch({ type: 'DATA_QUERY', payload: jsonResponse });
            console.warn("DATA_QUERY---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}