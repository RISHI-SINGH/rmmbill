import { BASE_URL } from '../utils';

export const dispositionAction = (data,token) => {
     console.warn("dispotionAction-->",data,token)
    try {
        return async (dispatch) => {
            const response = await fetch(`${BASE_URL}/rm/getdispositions`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                },
                 body: JSON.stringify(data)
            });

            const jsonResponse = await response.json();
            
            dispatch({ type: 'DISPOSITION', payload: jsonResponse });
            console.warn("dispotionAction---->",jsonResponse)
        }
    } catch (error) {
        console.warn('error===>', error);
    }
}