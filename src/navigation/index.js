import React, {useEffect} from 'react';
import {ActivityIndicator} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AfterLogin from '../navigation/AfterLogin';
import BeforeLogin from '../navigation/Beforelogin';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import * as loginAction from '../action/loginAction';

import Overdue from '../screen/ForgotPassword';

import {useState} from 'react';
import Login from '../screen/Login';
import Home from '../screen/Home';

const Stack = createStackNavigator();

function NavBar(props) {
  useEffect(() => {
    console.warn('INDEXXXX->', props.userTokenReducer);
    console.warn('bool', bool);
  });

  useEffect(() => {
    AsyncStorage.getItem('@user_token').then(token => {
      console.warn('token-->', token);
      if (token === null) {
        setBool(false);
        props.userToken('');
      } else {
        setBool(false);
        props.userToken(token);
      }
    });
  }, []);

  const [token, setToken] = useState('');
  const [bool, setBool] = useState(true);

  // return props.userTokenReducer !== '' ? (
  //   <Stack.Navigator headerMode="none">
  //     <Stack.Screen name="Home" component={AfterLogin} />
  //   </Stack.Navigator>
  // ) : (
  //   <Stack.Navigator headerMode="none">
  //     <Stack.Screen name="login" component={BeforeLogin} />
  //   </Stack.Navigator>
  // );
  return bool ? (
    <ActivityIndicator size="large" color="orange" />
  ) : props.userTokenReducer ? (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Home" component={AfterLogin} />
    </Stack.Navigator>
  ) : (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="login" component={BeforeLogin} />
    </Stack.Navigator>
  );
}

const mapStateToProps = state => {
  return {
    userTokenReducer: state.userTokenReducer,
  };
};

export default connect(
  mapStateToProps,
  {...loginAction},
)(NavBar);
