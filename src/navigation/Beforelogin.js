import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screen/Login';
import ForgotPassword from '../screen/ForgotPassword';
import VerifyOTP from '../screen/VerifyOTP';
import ResetPassword from '../screen/ResetPassword';


const StackNav = createStackNavigator();

export default function BeforeLogin() {
  return (
    <StackNav.Navigator headerMode='none'>
      <StackNav.Screen name="Login" component={Login} />
      <StackNav.Screen name="ForgotPassword" component={ForgotPassword} />
      <StackNav.Screen name="VerifyOTP" component={VerifyOTP} />
      <StackNav.Screen name="ResetPassword" component={ResetPassword} />
    
    </StackNav.Navigator>
  )
}
