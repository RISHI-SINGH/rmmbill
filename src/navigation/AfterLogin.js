import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screen/Home';
import Overdue from '../screen/ForgotPassword';
import TaskDetail from '../screen/TaskDetail';
import CustomDrawer from '../component/CustomDrawer';
import ChangePassword from '../screen/DrawerScreen/ChangePassword';
import Completed from '../screen/ResetPassword';
import AssignToRM from '../screen/VerifyOTP';
import TaskDetail4 from '../screen/TaskDetail4';
import ViewInvoice from '../screen/ViewInvoice';
import CalendarTask from '../screen/CalendarTask';
import Test from '../screen/Test';
//  import Login from '../screen/Login';

const StackNav = createStackNavigator();
const Drawer = createDrawerNavigator();

export default function AfterLogin() {
  return (
    <StackNav.Navigator headerMode="none">
      <StackNav.Screen name="Home" component={Home} />
      <StackNav.Screen name="TaskDetail" component={TaskDetail} />
      <StackNav.Screen name="Overdue" component={Overdue} />
      <StackNav.Screen name="AssignToRM" component={AssignToRM} />
      <StackNav.Screen name="Completed" component={Completed} />
      <StackNav.Screen name="TaskDetail4" component={TaskDetail4} />
      <StackNav.Screen name="ViewInvoice" component={ViewInvoice} />
      <StackNav.Screen name="CalendarTask" component={CalendarTask} />
      <StackNav.Screen name="Test" component={Test} />
      {/* <StackNav.Screen name="Login" component={Login}/> */}
    </StackNav.Navigator>
  );
}

const DrawerNav = () => {
  return (
    <Drawer.Navigator
      drawerType="slide"
      headerMode="none"
      drawerContent={props => <CustomDrawer {...props} />}>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="ChangePassword" component={ChangePassword} />
    </Drawer.Navigator>
  );
};
