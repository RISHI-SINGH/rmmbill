import {View, Text, TextInput, StyleSheet} from 'react-native';
import React, {Component} from 'react';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class InputText extends Component {
  runFunc = value => {
    this.props.func(value);
  };

  onBlurRunFunc = () => {
    this.props.onChangeTextValue();
  };

  render() {
    return (
      <View style={{marginTop: hp(this.props.mTop)}}>
        {this.props.formText !== undefined && (
          <Text style={styles.text}>{this.props.formText}</Text>
        )}
        <TextInput
          keyboardType={
            this.props.keyboardType !== undefined
              ? this.props.keyboardType
              : null
          }
          value={this.props.value}
          style={
            this.props.style !== undefined
              ? this.props.style.input
              : styles.input
          }
          editable={this.props.editable === false ? false : true}
          //editable={this.props.edit}
          onChangeText={value => this.runFunc(value)}
          onEndEditing={() => {
            if (this.props.onChangeTextValue !== undefined)
              this.onBlurRunFunc();
          }}
          secureTextEntry={this.props.secureTextEntry}
          autoCapitalize={this.props.autoCapitalize}
          maxLength={this.props.maxLength}
          returnKeyType={this.props.returnKeyType}
          placeholder={this.props.placeholder}
         
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    fontSize: 16,
    fontWeight: '300',
   marginTop:hp("2%"),
   alignSelf:'center',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default InputText;
