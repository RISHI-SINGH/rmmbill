import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

class Button extends Component {
  // runFunc = () => {
  //   this.props.func();
  // };

  render() {
    return (
      <TouchableOpacity
        // onPress={() => this.runFunc()}
        onPress={this.props.onPress}
        style={[
          styles.container,
          {
            height: hp(this.props.height),
            backgroundColor: this.props.bColor,
            width: this.props.width,
            marginTop: hp(this.props.mTop),
          },
        ]}>
        {this.props.activityIndicator ? (
          <ActivityIndicator size="large" color="white" />
        ) : (
          <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
            {this.props.firsttext ? (
              <Text style={{color: 'white', fontWeight:'bold', fontSize: this.props.firsttextSize, marginRight:10}}>
                {this.props.firsttext}
              </Text>
            ) : null}
            <Text style={{color: 'white', fontSize: this.props.textSize}}>
              {this.props.text}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
    marginBottom: hp('1%'),
  },
});

export default Button;
