import {Text, StyleSheet} from 'react-native';
import React, {Component} from 'react';

class ErrMsg extends Component {
  render() {
    return <Text style={styles.error}>{this.props.errMsg}</Text>;
  }
}

const styles = StyleSheet.create({
  error: {
    alignSelf: 'center',
    marginLeft: 20,
    color: 'red',
    textAlign: 'center',
  },
});

export default ErrMsg;
