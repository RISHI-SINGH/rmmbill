import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Platform,
  Alert,
  BackHandler,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {images, alert} from '../../utils';

const BACK_ICON = images.backIcon;
const HAM_ICON = images.hamIcon;
const WHITE_BACK_ICON = images.whiteBackIcon;
const NOTIFICATION_ICON = images.logoutIcon;

class Header extends Component {
  leftIconFunc = icon => {
    if (icon === 'chevronLeft') {
      this.props.navigation.goBack();
    } else if (icon === 'ham') {
      this.props.navigation.openDrawer();
    } else {
      return null;
    }
  };

  leftIconImage = (icon, color) => {
    if (icon === 'chevronLeft') {
      if (color === 'white') {
        return (
          <Image
            source={icon === 'chevronLeft' ? WHITE_BACK_ICON : null}
            style={styles.leftIconImage}
          />
        );
      } else {
        return (
          <Image
            source={icon === 'chevronLeft' ? BACK_ICON : null}
            style={styles.leftIconImage}
          />
        );
      }
    } else if (icon === 'ham') {
      return (
        <Image
          source={icon === 'ham' ? HAM_ICON : null}
          style={styles.leftIconImage}
        />
      );
    } else {
      return null;
    }
  };

  rightIconImage = icon => {
    if (icon === 'notification') {
      return (
        <Image
          source={icon === 'notification' ? NOTIFICATION_ICON : null}
          style={styles.leftIconImage}
        />
      );
    } else {
      return null;
    }
  };

  render() {
    const {
      leftIcon,
      headingName,
      rightIcon,
      leftIconColor,
      headingColor,
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.leftIcon}>
          <TouchableOpacity
            style={{width: wp('7%'), height: hp('4%')}}
            onPress={() => this.leftIconFunc(leftIcon)}>
            {this.leftIconImage(leftIcon, leftIconColor)}
          </TouchableOpacity>
        </View>

        <View style={styles.heading}>
          {headingName ? (
            <Text
              style={[
                styles.headingName,
                {color: headingColor === 'white' ? 'white' : 'black'},
              ]}>
              {headingName}
            </Text>
          ) : null}
        </View>
        {rightIcon && (
          <View style={styles.rightIcon}>
            <TouchableOpacity
              style={{width: wp('7%'), height: hp('4%')}}
              onPress={() =>
                alert(
                  'Are you sure you want to logout ?',
                  '',
                  'OK',
                  async () => {
                    // await AsyncStorage.removeItem('@user_token'),
                    //   await AsyncStorage.removeItem('@user_data'),
                    this.props.navigation.navigate('Login');
                  },
                )
              }>
              {this.rightIconImage(rightIcon)}
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    marginTop: Platform.OS === 'ios' ? hp('4%') : hp('0%'),
  },
  leftIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    flex: 5,
    justifyContent: 'center',
  },
  headingName: {
    fontSize: 24,
  },
  rightIcon: {
    flex: 1,
    justifyContent: 'center',
  },
  leftIconImage: {
    resizeMode: 'contain',
    width: '100%',
    height: '100%',
  },
});

export default Header;
