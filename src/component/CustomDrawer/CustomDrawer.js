import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Dimensions,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../Header';
import {alert, images, colors} from '../../utils';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import * as loginAction from '../../action/loginAction';
import {connect} from 'react-redux';

const {
  sidebarOne,
  sidebarTwo,
  sidebarThree,
  sidebarFour,
  sidebarFive,
  sidebarSix,
  sidebarSeven,
  sidebarEight,
  sidebarNine,
  homeIcon,
  backBannerThree,
  logoutIcon,
} = images;

const {background} = colors;

class CustomDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {
          name: 'Home',
          type: 'screen',
          value: 'Home',
          image: homeIcon,
        },

        {
          name: 'Change Password',
          type: 'screen',
          value: 'ChangePassword',
          image: sidebarSeven,
        },

        {
          name: 'Logout',
          type: 'logout',
          value: 'Login',
          image: logoutIcon,
        },
      ],
      userData: [],
      userImage: '',
    };
  }

  menuFunc = (type, screen, screenProps, type1, name) => {
    console.warn('type-->', type1, type);
    if (type === 'screen') {
      this.props.navigation.navigate(screen, screenProps);
    } else if (type1 === 'screen1') {
      Alert.alert(name, 'Coming Soon', [{text: 'OK'}], {cancelable: false});
    } else {
      alert('Are you sure you want to logout ?', '', 'OK', async () => {
        this.props.userToken(null);
        // await AsyncStorage.removeItem('@user_token'),
        //   await AsyncStorage.removeItem('@user_data'),
        AsyncStorage.removeItem('@user_token');
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <ImageBackground
            source={backBannerThree}
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: background,
            }}>
            <View style={{flex: 2}}>
              <Header
                leftIcon="chevronLeft"
                leftIconColor="white"
                headingName="Menu"
                headingColor="white"
                {...this.props}
              />
            </View>
            <View style={{flex: 4, flexDirection: 'row'}}>
              <View
                style={{
                  width: wp('45%'),
                  paddingLeft: 10,
                  paddingTop: 10,
                  marginLeft: 10,
                }}>
                <Text numberOfLines={1} style={{fontSize: 16, color: 'white'}}>
                  "Rishi"
                </Text>
                <Text
                  numberOfLines={2}
                  style={{fontSize: 12, fontWeight: '300', color: 'white'}}>
                  "No email"
                </Text>
              </View>
              <View
                style={{
                  borderRadius:
                    Math.round(
                      Dimensions.get('window').width +
                        Dimensions.get('window').height,
                    ) / 2,
                  width: Dimensions.get('window').width * 0.16,
                  height: Dimensions.get('window').width * 0.16,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  marginLeft: wp('3%'),
                  marginTop: hp('3%'),
                }}>
                <Image
                  // source={{uri: this.state.userImage,cache: 'reload'}}
                  source={{uri: this.state.userImage + '?' + new Date()}}
                  // source={{uri:"http://mbill.n1.iworklab.com:3210/images/userImages/1.png"}}
                  style={{
                    borderRadius:
                      Math.round(
                        Dimensions.get('window').width +
                          Dimensions.get('window').height,
                      ) / 2,
                    width: Dimensions.get('window').width * 0.15,
                    height: Dimensions.get('window').width * 0.15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'white',
                  }}
                />
              </View>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.body}>
          <View style={styles.menuContainer}>
            <FlatList
              data={this.state.menu}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() =>
                    this.menuFunc(
                      item.type,
                      item.value,
                      {},
                      item.type1,
                      item.name,
                    )
                  }
                  style={{
                    flexDirection: 'row',
                    marginTop: hp('4%'),
                    borderBottomColor: '#d9d9d9',
                    borderBottomWidth: 2,
                  }}>
                  <View
                    style={{
                      flex: 1,
                    }}>
                    <Image
                      source={item.image}
                      resizeMode="contain"
                      style={{
                        width: wp('6%'),
                        height: hp('3%'),
                        marginLeft: 5,
                        marginBottom: 10,
                      }}
                    />
                  </View>
                  <View style={{flex: 4, justifyContent: 'center'}}>
                    <Text style={{fontSize: 16}}>{item.name}</Text>
                    <Text style={{fontSize: 12, color: 'grey'}}>{item.x}</Text>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: background,
  },
  header: {
    flex: 1,
    backgroundColor: 'grey',
  },
  body: {
    flex: 3,
    alignItems: 'center',
  },
  menuContainer: {
    flex: 1,
    width: wp('60%'),
  },
});

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
  };
};

export default connect(
  mapStateToProps,
  {...loginAction},
)(CustomDrawer);
