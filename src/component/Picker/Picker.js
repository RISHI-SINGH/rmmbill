import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
class Picker extends Component {
  runFunc = value => {
    this.props.pickerFunc(value);
  };

  render() {
    return (
      <View
        style={{
          marginTop: hp(this.props.mTop),
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={styles.text}>{this.props.formText}</Text>
        <View style={styles.picker}>
          <RNPickerSelect
            placeholder={{}}
            style={genderPicker}
            onValueChange={value => this.runFunc(value)}
            value={
              this.props.value !== '' ||
              this.props.value !== null ||
              this.props.value !== undefined
                ? this.props.value
                : null
            }
            items={this.props.data}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  picker: {
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    width: wp('80%'),
  },
});

const genderPicker = StyleSheet.create({
  placeholder: {
    color: 'grey',
    fontSize: hp('2.3%'),
    paddingVertical: hp('1.5%'),
    paddingLeft: 8,
  },
  inputIOS: {
    color: 'grey',
    fontSize: hp('2.3%'),
    paddingVertical: hp('1.5%'),
    paddingLeft: 8,
    width: wp('80%'),
  },
  inputAndroid: {
    color: 'grey',
    fontSize: hp('2.3%'),
    paddingVertical: hp('0.9%'),
    paddingLeft: 8,

    width: wp('80%'),
  },
});
export default Picker;
