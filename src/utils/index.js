import {Alert} from 'react-native';

export const images = {
  logo: require('../assets/logo.png'),
  backIcon: require('../assets/black_back_button.png'),
  whiteBackIcon: require('../assets/white_back_button.png'),
  hamIcon: require('../assets/menu.png'),
  homeIcon: require('../assets/home.png'),
  sidebarOne: require('../assets/icon_1.png'),
  sidebarTwo: require('../assets/icon_2.png'),
  sidebarThree: require('../assets/icon_3.png'),
  sidebarFour: require('../assets/icon_4.png'),
  sidebarFive: require('../assets/icon_5.png'),
  sidebarSix: require('../assets/icon_6.png'),
  sidebarSeven: require('../assets/icon_7.png'),
  sidebarEight: require('../assets/icon_8.png'),
  sidebarNine: require('../assets/icon_9.png'),
  searchInvoice: require('../assets/icon_12.png'),
  sendSMS: require('../assets/icon_13.png'),
  salesReport: require('../assets/icon_14.png'),
  profitLossReport: require('../assets/icon_15.png'),
  expenses: require('../assets/icon_16.png'),
  ledgerDetail: require('../assets/icon_17.png'),
  manageInventory: require('../assets/icon_18.png'),
  billing: require('../assets/icon_19.png'),
  backBannerOne: require('../assets/back_banner_1.png'),
  backBannerTwo: require('../assets/back_banner_2.png'),
  backBannerThree: require('../assets/back_banner.png'),
  notification: require('../assets/notification.png'),
  greenRoundBox: require('../assets/icon_10.png'),
  robot: require('../assets/icon_11.png'),
  white_mbill: require('../assets/white_mbill.png'),
  pencil: require('../assets/pencil.png'),
  search: require('../assets/search.png'),
  qrcode: require('../assets/icon_24.png'),
  rightIcon: require('../assets/icon_22.png'),
  customerDetail: require('../assets/icon_29.png'),
  addProduct: require('../assets/icon_31.png'),
  scanProduct: require('../assets/icon_24.png'),
  searchProduct: require('../assets/icon_30.png'),
  imageIcon: require('../assets/icon_23.png'),
  rightIcon1: require('../assets/icon_26.png'),
  leftIcon1: require('../assets/icon_27.png'),
  deleteIcon: require('../assets/icon_32.png'),
  edit: require('../assets/icon_33.png'),
  Circle: require('../assets/icon_34.png'),
  selectedCircle: require('../assets/icon_35.png'),
  calendar: require('../assets/calendar_1.png'),
  expenseImage: require('../assets/back_banner_3.png'),
  plane: require('../assets/icon_63.png'),
  bellSolid: require('../assets/icon_39.png'),
  bellHollow: require('../assets/icon_36.png'),
  dummy: require('../assets/dummycard.png'),
  assignedIcon: require('../assets/icon_68.png'),
  overdueIcon: require('../assets/icon_67.png'),
  assignRM: require('../assets/icon_69.png'),
  Completed: require('../assets/icon_70.png'),
  logoutIcon: require('../assets/icon_66.png'),
  phone: require('../assets/icon_64.png'),
  location: require('../assets/icon_65.png'),
};

export const colors = {
  red: '#ED502A',
  fbBlue: '#3B5998',
  googlePink: '#FF6464',
  grey: '#61605D',
  background: '#FAFAFA',
};

export const alert = (mainMsg, otherMsg, okText, func) => {
  Alert.alert(
    mainMsg,
    otherMsg !== '' ? otherMsg : '',
    [
      {
        text: okText,
        onPress: func,
      },
      {
        text: 'Cancel',
        style: 'cancel',
      },
    ],
    {cancelable: true},
  );
};

// export const BASE_URL = 'http://mbill.n1.iworklab.com:3210/api';
//export const BASE_URL = 'http://mbill.n1.iworklab.com:3210/api/v2';
export const BASE_URL =
  'https://api-dot-feisty-audio-278013.el.r.appspot.com/api/v2';
