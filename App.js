import React, {Component} from 'react';
import {
  View,
  Text,
  PermissionsAndroid,
  Platform,
  Alert,
  StatusBar,
} from 'react-native';
import NavBar from './src/navigation/index';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import store from './src/reducer';
// import Geolocation from 'react-native-geolocation-service';
import Geolocation from '@react-native-community/geolocation';
import SplashScreen from 'react-native-splash-screen';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLongitude: 'unknown', //Initial Longitude
      currentLatitude: 'unknown', //Initial Latitude
      initialPosition: '',
      lastPosition: '',
    };
  }

  componentDidMount() {
     SplashScreen.hide();
    Geolocation.getCurrentPosition(
      position => {
        const initialPosition = JSON.stringify(position);
        this.setState({initialPosition});
      },
      error => Alert.alert('Error', JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 2000000000, maximumAge: 1000000000},
    );
    this.watchID = Geolocation.watchPosition(position => {
      const lastPosition = JSON.stringify(position);
      this.setState({lastPosition});
    });
  }

  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }

  // async componentDidMount() {
  //   if(Platform.OS === 'ios'){
  //        this.getCurrentLocation();
  //      }
  //      else{
  //        try {
  //         const granted = await PermissionsAndroid.request(
  //           PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //           {
  //             title: 'Device current location permission',
  //             message:
  //               'Allow app to get your current location',

  //             buttonPositive: 'OK',
  //           },
  //         );
  //         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //           this.getCurrentLocation();
  //         } else {
  //           console.warn('Location permission denied');
  //         }
  //       } catch (err) {
  //         console.warn(err);
  //       }
  //      }
  //  }

  //  getCurrentLocation(){
  //   Geolocation.setRNConfiguration({ authorizationLevel : "whenInUse" });
  //      Geolocation.requestAuthorization()
  //      Geolocation.getCurrentPosition(
  //         (position) => {
  //             console.warn("current Loc-->",position);
  //         },
  //         (error) => {
  //           console.warn("map error: ",error);
  //             console.warn(error.code, error.message);
  //         },
  //         { enableHighAccuracy: false, timeout: 15000, maximumAge: 10000 }
  //     );
  //    }

  render() {
    console.warn('locationnnnn-->', this.state.initialPosition);
    return (
      <Provider store={store}>
        <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
        <NavigationContainer>
          <NavBar />
        </NavigationContainer>
      </Provider>
    );
  }
}

export default App;
